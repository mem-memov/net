#include "Factory.h"
#include <stdlib.h>
#include "Answers.h"
#include "Authors.h"
#include "Characters.h"
#include "Numbers.h"
#include "Objects.h"
#include "Parameters.h"
#include "Phrases.h"
#include "Questions.h"
#include "Triplets.h"
#include "Types.h"
#include "Values.h"
#include "Words.h"

struct Factory {
	struct Answers * answers; 
	struct Authors * authors;
	struct Characters * characters;
	struct Database * database;
	struct Messages * messages;
	struct Numbers * numbers;
	struct Objects * objects;
	struct Parameters * parameters;
	struct Parsers * parsers;
	struct Phrases * phrases;
	struct Questions * questions;
	struct Triplets * triplets;
	struct Types * types;
	struct Values * values;
	struct Words * words;
};

struct Factory * Factory_construct(struct Database * database)
{
	struct Factory * this = malloc(sizeof (struct Factory));
	
	this->database = database;
	
	this->types = Types_construct(database);
	this->numbers = Numbers_construct(this->types);
	this->characters = Characters_construct(database, this->numbers);
	this->words = Words_construct(this->characters, database);
	this->phrases = Phrases_construct(this->words, database);
	this->authors = Authors_construct(this->phrases, database);
	this->objects = Objects_construct(this->phrases, database);
	this->parameters = Parameters_construct(this->phrases, database);
	this->values = Values_construct(this->phrases, database);
	this->triplets = Triplets_construct(this->objects, this->parameters, this->values, database);
	this->answers = Answers_construct(this->triplets, database);
	this->questions = Questions_construct(this->triplets, database);
	
	this->messages = NULL;
	this->parsers = NULL;

	return this;
}

void * Factory_destruct(struct Factory * this)
{
	if (NULL != this) {
		
		this->types = Types_destruct(this->types);
		this->numbers = Numbers_destruct(this->numbers);
		this->characters = Characters_destruct(this->characters);
		this->words = Words_destruct(this->words);
		this->authors = Authors_destruct(this->authors);
		this->phrases = Phrases_destruct(this->phrases);
		this->objects = Objects_destruct(this->objects);
		this->parameters = Parameters_destruct(this->parameters);
		this->values = Values_destruct(this->values);
		this->triplets = Triplets_destruct(this->triplets);
		this->answers = Answers_destruct(this->answers );
		this->questions = Questions_destruct(this->questions);
		
		this->messages = Messages_destruct(this->messages);
		this->parsers = Parsers_destruct(this->parsers);
		
		free(this);
	}

	return NULL;
}

struct Messages * Factory_makeMessages(struct Factory * this)
{
	if ( this->messages == NULL ) {
		this->messages = Messages_construct(this->authors, this->answers, this->questions, this->database);
	}
	
	return this->messages;
}

struct Parsers * Factory_makeParsers(struct Factory * this)
{
	if ( this->parsers == NULL ) {
		this->parsers = Parsers_construct();
	}
	
	return this->parsers;
}

struct Types * Factory_makeTypes(struct Factory * this)
{
	return this->types;
}
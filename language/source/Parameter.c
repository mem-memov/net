#include "Parameter.h"

struct Parameter
{
	size_t id;
	struct Phrases * phrases;
};

struct Parameter * Parameter_construct(size_t id, struct Phrases * phrases)
{
	struct Parameter * this = malloc(sizeof(struct Parameter));

	this->id = id;
	this->phrases = phrases;
	
	return this;
}

void * Parameter_destruct(struct Parameter * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

#include "../source/CountError.h"

#define COUNTERROR_MAX_CALLS 1

struct CountError
{
	char call;
	char * method[COUNTERROR_MAX_CALLS];
	void * destructed[COUNTERROR_MAX_CALLS];
	size_t value[COUNTERROR_MAX_CALLS];
};

struct CountError * CountError_mock()
{
	struct CountError * this = malloc(sizeof(struct CountError));
	
	this->call = 0;
	
	char i;
	for (i = 0; i < COUNTERROR_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->value[i] = 55555;
	}

	return this;
}

void * CountError_unmock(struct CountError * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

void * CountError_destruct(struct CountError * this)
{
	this->method[this->call] = "CountError_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void CountError_forbidNegativeValueWhenDecrementing(struct CountError * this, size_t value)
{
	this->method[this->call] = "CountError_forbidNegativeValueWhenDecrementing";
	this->value[this->call] = value;
	
	this->call++;
}

void CountError_forbidOverflowWhenIncrementing(struct CountError * this, size_t value)
{
	this->method[this->call] = "CountError_forbidOverflowWhenIncrementing";
	this->value[this->call] = value;
	
	this->call++;
}
#ifndef OBJECT_H
#define OBJECT_H

#include <stdlib.h>
#include "Phrases.h"

struct Object;

struct Object * Object_construct(size_t id, struct Phrases * phrases);

void * Object_destruct(struct Object * this);

#endif

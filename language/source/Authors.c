#include "Authors.h"
#include <stdlib.h>
#include "WordParser.h"
#include "Phrase.h"

struct Authors
{
	struct Phrases * phrases;
	struct Database * database;
};

struct Authors * Authors_construct(struct Phrases * phrases, struct Database * database)
{
	struct Authors * this = malloc(sizeof(struct Authors));

	this->phrases = phrases;
	
	this->database = database;
	
	return this;
}

void * Authors_destruct(struct Authors * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Author * Authors_make(struct Authors * this, struct Parser * parser)
{
	struct WordParser * wordParser = Parser_makeWordParser(parser, 0);

	struct Phrase * phrase = Phrases_make(this->phrases, wordParser);
	
	size_t id = 1;

	return Author_construct(id, this->phrases);
}
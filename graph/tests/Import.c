#include "../source/Import.h"
#include <stdlib.h>

#define IMPORT_MAX_CALLS 2

struct Import {
	char call;
	char * method[IMPORT_MAX_CALLS];
	void * destructed[IMPORT_MAX_CALLS];
	FILE * file[IMPORT_MAX_CALLS];
};

struct Import * Import_mock()
{
	struct Import * this = malloc(sizeof (struct Import));

	this->call = 0;
	
	char i;
	for (i = 0; i < IMPORT_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->file[i] = NULL;
	}
	
	return this;
}

void * Import_unmock(struct Import * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void * Import_destruct(struct Import * this)
{
	this->method[this->call] = "Import_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void Import_read(struct Import * this, FILE * file)
{
	this->method[this->call] = "Import_read";
	this->file[this->call] = file;
	
	this->call++;
}
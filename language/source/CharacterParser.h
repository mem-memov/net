#ifndef CHARACTERPARSER_H
#define CHARACTERPARSER_H

#include "Character.h"
#include "Characters.h"

struct CharacterParser;

struct CharacterParser * CharacterParser_construct();

void * CharacterParser_destruct(struct CharacterParser * this);

void CharacterParser_makeCharacters(
    struct CharacterParser * this, 
    struct Character *** characterList, 
    int * characterCount,
    struct Characters * characters
);

#endif

#ifndef VALUES_H
#define VALUES_H

#include "Database.h"
#include "Parser.h"
#include "Phrases.h"
#include "Value.h"

struct Values;

struct Values * Values_construct(struct Phrases * phrases, struct Database * database);

void * Values_destruct(struct Values * this);

struct Value * Values_make(struct Values * this, struct Parser * parser);

#endif

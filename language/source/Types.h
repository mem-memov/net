#ifndef TYPES_H
#define TYPES_H

struct Types;

struct Types * Types_construct();

void * Types_destruct(struct Types * this);

void Types_occupyNodes(struct Types * this);

struct Type * Types_makeZero(struct Types * this);

struct Type * Types_makeOne(struct Types * this);

struct Type * Types_makeTwo(struct Types * this);

struct Type * Types_makeThree(struct Types * this);

struct Type * Types_makeFour(struct Types * this);

struct Type * Types_makeFive(struct Types * this);

struct Type * Types_makeSix(struct Types * this);

struct Type * Types_makeSeven(struct Types * this);

struct Type * Types_makeEight(struct Types * this);

struct Type * Types_makeNine(struct Types * this);

struct Type * Types_makeMessage(struct Types * this);

struct Type * Types_makeQuestion(struct Types * this);

struct Type * Types_makeTriplet(struct Types * this);

struct Type * Types_makeObject(struct Types * this);

struct Type * Types_makeParameter(struct Types * this);

struct Type * Types_makeAuthor(struct Types * this);

struct Type * Types_makePhrase(struct Types * this);

struct Type * Types_makeWord(struct Types * this);

struct Type * Types_makeCharacter(struct Types * this);

#endif

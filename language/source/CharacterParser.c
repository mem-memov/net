#include "CharacterParser.h"
#include <stdlib.h>
#include <string.h>

struct CharacterParser {
	char * string;
	size_t length;
	char separator;
};

struct CharacterParser * CharacterParser_construct(char * string)
{
	struct CharacterParser * this = malloc(sizeof (struct CharacterParser));

	this->length = strlen(string);
	
	this->string = malloc(sizeof(char) * (this->length + 1));
	
	strcpy(this->string, string);
	
	this->separator = '_';
	
	return this;
}

void * CharacterParser_destruct(struct CharacterParser * this)
{
	if (NULL != this) {
		free(this->string);
		free(this);
	}

	return NULL;
}

void CharacterParser_makeCharacters(
	struct CharacterParser * this, 
	struct Character *** characterList, 
	int * characterCount,
	struct Characters * characters
) {
	// initialize character count
	
	if ( this->length == 0 ) {
		(*characterCount) = 0;
	} else {
		(*characterCount) = 1;
	}
	
	// count characters
	
	size_t i;
	
	for (i = 0; i < this->length; i++) {
		
		if ( this->string[i] == this->separator) {
			++(*characterCount);
		}
	}
	
	// find characters
	
	int characterIndex;
	
	char ** characterFirstNumbers = malloc(sizeof(char *) * (*characterCount));
	size_t * characterLengths = malloc(sizeof(size_t) * (*characterCount));
	
	for (characterIndex = 0; characterIndex < (*characterCount); ++characterIndex) {
		characterFirstNumbers[characterIndex] = NULL;
		characterLengths[characterIndex] = 0;
	}

	characterIndex = 0;
	for (i = 0; i < this->length; i++) {
		
		if ( this->string[i] == this->separator) {
			++characterIndex;
			continue;
		}
		
		if ( characterLengths[characterIndex] == 0 ) {
			characterFirstNumbers[characterIndex] = this->string + i;
		}
		
		++characterLengths[characterIndex];
	}
	
	// create character structs
	
	(*characterList) = malloc(sizeof(struct Character) * (*characterCount));
	
	int numberIndex;
	size_t characterLength;
	char * firstNumber;
	
	for (characterIndex = 0; characterIndex < (*characterCount); ++characterIndex) {
		
		characterLength = characterLengths[characterIndex];
		firstNumber = characterFirstNumbers[characterIndex];
		
		char * characterNumbers = malloc(sizeof(char) * (characterLength + 1));
		characterNumbers[characterLength] = '\0';

		for (numberIndex = 0; numberIndex < characterLength; ++numberIndex ) {
			characterNumbers[numberIndex] = firstNumber[numberIndex];
		}
		
		*((*characterList) + characterIndex) = Characters_make(characters, characterNumbers);
		
		free(characterNumbers);
	}
}
#ifndef DATABASE_H
#define DATABASE_H

#include <stdlib.h>

struct Database;

struct Database * Database_construct(
    void * store,
    void * collections
);

void * Database_destruct
(struct Database * this);

struct Collection * Database_collectNode
(struct Database * this, size_t node);

struct Collection * Database_createNode
(struct Database * this, struct Collection * origins, struct Collection * destinations);

struct Collection * Database_readDestinationsOfNode
(struct Database * this, size_t origin);

struct Collection * Database_readOriginsOfNode
(struct Database * this, size_t destination);


#endif
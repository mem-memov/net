#include "Connection.h"
#include "Error.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>

#include <stdio.h>

struct Connection {
    int descriptor;
    struct Request * request;
    char * response;
};

struct Connection * Connection_construct(int descriptor, struct Request * request)
{
	struct Connection * this = malloc(sizeof(struct Connection));

	this->descriptor = descriptor;
	this->request = request;
	this->response = NULL;

	return this;
}

void * Connection_destruct(struct Connection * this)
{
	if ( NULL != this ) {
		this->request = Request_destruct(this->request);

		if ( NULL != this->response ) {
			free(this->response);
		}

		free(this);
	}

	return NULL;
}

void Connection_close(struct Connection * this)
{
    close(this->descriptor);
}

void Connection_receive(struct Connection * this)
{
	if ( NULL != this->response ) {
		exit(1);
	}
	
	if ( ! Request_isClean(this->request) ) {
		exit(1);
	}

    int receivedLength;

	long int bufferLength = sizeof(unsigned char) * 1024;
	unsigned char * buffer = malloc(bufferLength);

    do {
        receivedLength = recv(this->descriptor, buffer, bufferLength, 0);
		
		if ( -1 == receivedLength) {
			exit(1);
		}
		
		Request_appendChunk(this->request, buffer, receivedLength);
		
		if ( Request_isComplete(this->request) ) {
			break;
		}
		
    } while (receivedLength > 0);
	
	free(buffer);
}

void Connection_apply(struct Connection * this, struct Application * application)
{
	if ( NULL != this->response ) {
		exit(1);
	}
	
	if ( Request_isClean(this->request) ) {
		exit(1);
	}
	
	this->response = Application_execute(application, Request_getBody(this->request));
	
	Request_clean(this->request);
}

void Connection_send(struct Connection * this)
{	
	if ( NULL == this->response ) {
		exit(1);
	}
	
	if ( ! Request_isClean(this->request) ) {
		exit(1);
	}

    int sendResult = send(this->descriptor, this->response, strlen(this->response)-1, 0);

    Error_inConnectionAfterSending(sendResult);

	free(this->response);
	this->response = NULL;
}

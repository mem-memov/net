#ifndef LANGUAGES_H
#define LANGUAGES_H

#include "Language.h"
#include "Database.h"

struct Languages;

struct Languages * Languages_construct
(struct Database * database);

void * Languages_destruct
(struct Languages * this);

struct Language * Languages_make
(struct Languages * this);

#endif

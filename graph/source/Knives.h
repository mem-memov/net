#ifndef KNIVES_H
#define KNIVES_H

#include "Boat.h"
#include "Knife.h"

struct Knives;

struct Knives * Knives_construct();

void * Knives_destruct(struct Knives * this);

struct Knife * Knives_make(struct Knives * this, struct Boat * boat);

#endif

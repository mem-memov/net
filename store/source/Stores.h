#ifndef STORES_H
#define STORES_H

// Store.h and Stores.h get concatenated in Makefile
#ifndef STORE_H
#include "Store.h"
#endif

struct Stores;

struct Stores * Stores_construct
();

void * Stores_destruct
(struct Stores * this);

struct Store * Stores_make
(struct Stores * this, size_t graphSize, size_t placeSize);

#endif

#include "Knife.h"
#include <stdlib.h>

struct Knife
{
	struct Boat * boat;
};

struct Knife * Knife_construct(struct Boat * boat)
{
	struct Knife * this = malloc(sizeof(struct Knife));
	
	this->boat = boat;

	return this;
}

void * Knife_destruct(struct Knife * this)
{
	if (NULL == this) {
		return NULL;
	}

	free(this);

	return NULL;
}

void Knife_cutDestination(struct Knife * this, struct Node * originNode, struct Node * destinationNode)
{
	size_t deletedOutgoingLink = Node_deleteDestination(originNode, Node_getPlace(destinationNode));

	if ( 0 == deletedOutgoingLink ) {
		return;
	}

	Node_deleteIncomingLink(destinationNode);

	Boat_deleteLinkEntry(this->boat, deletedOutgoingLink);
}

void Knife_cutOrigin(struct Knife * this, struct Node * originNode, struct Node * destinationNode)
{
	size_t deletedIncomingLink = Node_deleteOrigin(destinationNode, Node_getPlace(originNode));

	if ( 0 == deletedIncomingLink ) {
		return;
	}

	Node_deleteOutgoingLink(originNode);

	Boat_deleteLinkEntry(this->boat, deletedIncomingLink);
}
#ifndef WORDPARSER_H
#define WORDPARSER_H

#include "CharacterParser.h"

struct WordParser;

struct WordParser * WordParser_construct(char * string);

void * WordParser_destruct(struct WordParser * this);

void WordParser_makeCharacterParsers(
    struct WordParser * this, 
    struct CharacterParser *** characterParsers, 
    int * wordCount
);

#endif

#ifndef QUESTION_H
#define QUESTION_H

#include <stdlib.h>
#include "Triplets.h"

struct Question;

struct Question * Question_construct(size_t id, struct Triplets * triplets);

void * Question_destruct(struct Question * this);

#endif

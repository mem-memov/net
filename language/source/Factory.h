#ifndef FACTORY_H
#define FACTORY_H

#include "Database.h"
#include "Messages.h"
#include "Parsers.h"
#include "Types.h"

struct Factory;

struct Factory * Factory_construct(struct Database * database);

void * Factory_destruct(struct Factory * this);

struct Messages * Factory_makeMessages(struct Factory * this);

struct Parsers * Factory_makeParsers(struct Factory * this);

struct Types * Factory_makeTypes(struct Factory * this);

#endif

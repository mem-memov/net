#include "Numbers.h"
#include <stdlib.h>

struct Numbers {
	struct Types * types;
};

struct Numbers * Numbers_construct(struct Types * types)
{
	struct Numbers * this = malloc(sizeof (struct Numbers));
	
	this->types = types;

	return this;
}

void * Numbers_destruct(struct Numbers * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Number * Numbers_make(struct Numbers * this, char number)
{
	switch (number) {
		case '0':
			return Number_construct(number, Types_makeZero(this->types));
		case '1':
			return Number_construct(number, Types_makeOne(this->types));
		case '2':
			return Number_construct(number, Types_makeTwo(this->types));
		case '3':
			return Number_construct(number, Types_makeThree(this->types));
		case '4':
			return Number_construct(number, Types_makeFour(this->types));
		case '5':
			return Number_construct(number, Types_makeFive(this->types));
		case '6':
			return Number_construct(number, Types_makeSix(this->types));
		case '7':
			return Number_construct(number, Types_makeSeven(this->types));
		case '8':
			return Number_construct(number, Types_makeEight(this->types));
		case '9':
			return Number_construct(number, Types_makeNine(this->types));
		default:
			exit(1);
	}
}
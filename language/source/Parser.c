#include "Parser.h"
#include <stdlib.h>
#include <string.h>

struct Parser
{
	char * string;
	size_t length;
	char separator;
};

struct Parser * Parser_construct(char * string)
{
	struct Parser * this = malloc(sizeof(struct Parser));
	
	this->length = strlen(string);
	
	this->string = malloc(sizeof(char) * (this->length + 1));
	
	strcpy(this->string, string);
	
	this->separator = ':';

	return this;
}

void * Parser_destruct(struct Parser * this)
{
	if (NULL != this) {
		free(this->string);
		free(this);
	}

	return NULL;
}

char Parser_isQuestion(struct Parser * this)
{
	char lastCharacter = this->string[strlen(this->string) - 1];
	
	if ( lastCharacter == '?' ) {
		return 1;
	}
	
	return 0;
}

struct WordParser * Parser_makeWordParser(struct Parser * this, char phraseIndex)
{
	size_t i = 0;
	char phraseCount = 0;
	char * first = NULL;
	size_t length = 0;
	
	while ( i <= this->length ) {
		
		if ( this->string[i] == this->separator ) {
			++i;
			++phraseCount;
		}
		
		if (phraseCount > phraseIndex) {
			break;
		}

		if ( phraseCount == phraseIndex ) {
			if ( first == NULL ) {
				first = this->string + i;
			}
			++length;
		}
		
		++i;
	}

	char * string = malloc(sizeof(char) * (length + 1));
	string[length] = '\0';
	
	size_t k;
	
	for (k = 0; k < length; k++) {
		string[k] = first[k];
	}

	struct WordParser * wordParser = WordParser_construct(string);
	
	free(string);
	
	return wordParser;
}
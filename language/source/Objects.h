#ifndef OBJECTS_H
#define OBJECTS_H

#include "Database.h"
#include "Parser.h"
#include "Phrases.h"
#include "Object.h"

struct Objects;

struct Objects * Objects_construct(struct Phrases * phrases, struct Database * database);

void * Objects_destruct(struct Objects * this);

struct Object * Objects_make(struct Objects * this, struct Parser * parser);

#endif

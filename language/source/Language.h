#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <netdatabase.h>
#include "Messages.h"
#include "Parsers.h"

struct Language;

struct Language * Language_construct(
    struct Messages * messages, 
    struct Parsers * parsers
);

void * Language_destruct
(struct Language * this);

void Language_interpret
(struct Language * this, char * input, char ** output);

#endif

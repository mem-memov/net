#include "../source/Export.h"
#include <stdlib.h>

#define EXPORT_MAX_CALLS 1

struct Export
{
	char call;
	char * method[EXPORT_MAX_CALLS];
	void * destructed[EXPORT_MAX_CALLS];
	FILE * file[EXPORT_MAX_CALLS];
};

struct Export * Export_mock()
{
	struct Export * this = malloc(sizeof(struct Export));
	
	this->call = 0;
	
	char i;
	for (i = 0; i < EXPORT_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->file[i] = NULL;
	}

	return this;
}

void * Export_unmock(struct Export * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

void * Export_destruct(struct Export * this)
{
	this->method[this->call] = "Export_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void Export_write(struct Export * this, FILE * file)
{
	this->method[this->call] = "Export_write";
	this->file[this->call] = file;
	
	this->call++;
}
#include "Author.h"

struct Author
{
	size_t id;
	struct Phrases * phrases;
};

struct Author * Author_construct(size_t id, struct Phrase * phrases)
{
	struct Author * this = malloc(sizeof(struct Author));

	this->id = id;
	this->phrases = phrases;
	
	return this;
}

void * Author_destruct(struct Author * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}
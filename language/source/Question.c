#include "Question.h"

struct Question
{
	size_t id;
	struct Triplets * triplets;
};

struct Question * Question_construct(size_t id, struct Triplets * triplets)
{
	struct Question * this = malloc(sizeof(struct Question));

	this->id = id;
	this->triplets = triplets;
	
	return this;
}

void * Question_destruct(struct Question * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

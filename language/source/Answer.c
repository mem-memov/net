#include "Answer.h"

struct Answer
{
	size_t id;
	struct Triplets * triplets;
};

struct Answer * Answer_construct(size_t id, struct Triplets * triplets)
{
	struct Answer * this = malloc(sizeof(struct Answer));

	this->id = id;
	this->triplets = triplets;

	return this;
}

void * Answer_destruct(struct Answer * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

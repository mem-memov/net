#include "Language.h"
#include <stdlib.h>
#include "Message.h"
#include "Parser.h"

struct Language
{
	struct Messages * messages;
	struct Parsers * parsers;
};

struct Language * Language_construct(
	struct Messages * messages, 
	struct Parsers * parsers
) {
	struct Language * this = malloc(sizeof(struct Language));
	
	this->messages = messages;
	this->parsers = parsers;

	return this;
}

void * Language_destruct(struct Language * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void Language_interpret(struct Language * this, char * input, char ** output)
{
	struct Parser * parser = Parsers_make(this->parsers, input);
	
	struct Message * message = Messages_make(this->messages, parser);
	
	Message_write(message);
	
	//Message_respond(message, this->intelligence, output);
	
	message = Message_destruct(message);
}

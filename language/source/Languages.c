#include "Languages.h"
#include <stdlib.h>
#include "Database.h"
#include "Factory.h"

struct Languages
{
	struct Factory * factory;
};

struct Languages * Languages_construct(struct Database * database)
{
	struct Languages * this = malloc(sizeof(struct Languages));
	
	this->factory = Factory_construct(database);

	return this;
}

void * Languages_destruct(struct Languages * this)
{
	if (NULL != this) {
		// TODO
		free(this);
	}

	return NULL;
}

struct Language * Languages_make(struct Languages * this)
{
	struct Types * types = Factory_makeTypes(this->factory);
	Types_occupyNodes(types);
	
	return Language_construct(
		Factory_makeMessages(this->factory),
		Factory_makeParsers(this->factory)
	);
}
#include "../source/Knife.h"
#include <stdlib.h>

#define KNIFE_MAX_CALLS 2

struct Knife {
	char call;
	char * method[KNIFE_MAX_CALLS];
	void * destructed[KNIFE_MAX_CALLS];
	struct Node * originNode[KNIFE_MAX_CALLS];
	struct Node * destinationNode[KNIFE_MAX_CALLS];
};

struct Knife * Knife_mock()
{
	struct Knife * this = malloc(sizeof (struct Knife));

	this->call = 0;
	
	char i;
	for (i = 0; i < KNIFE_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->originNode[i] = NULL;
		this->destinationNode[i] = NULL;
	}
	
	return this;
}

void * Knife_unmock(struct Knife * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void * Knife_destruct(struct Knife * this)
{
	this->method[this->call] = "Knife_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void Knife_cutDestination(struct Knife * this, struct Node * originNode, struct Node * destinationNode)
{
	this->method[this->call] = "Knife_cutDestination";
	this->originNode[this->call] = originNode;
	this->destinationNode[this->call] = destinationNode;
	
	this->call++;
}

void Knife_cutOrigin(struct Knife * this, struct Node * originNode, struct Node * destinationNode)
{
	this->method[this->call] = "Knife_cutOrigin";
	this->originNode[this->call] = originNode;
	this->destinationNode[this->call] = destinationNode;
	
	this->call++;
}
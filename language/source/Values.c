#include "Values.h"
#include <stdlib.h>
#include "WordParser.h"

struct Values
{
	struct Phrases * phrases;
	struct Database * database;
};

struct Values * Values_construct(struct Phrases * phrases, struct Database * database)
{
	struct Values * this = malloc(sizeof(struct Values));

	this->phrases = phrases;
	
	this->database = database;
	
	return this;
}

void * Values_destruct(struct Values * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Value * Values_make(struct Values * this, struct Parser * parser)
{
	struct WordParser * wordParser = Parser_makeWordParser(parser, 3);
	
	struct Phrase * phrase = Phrases_make(this->phrases, wordParser);
	
	size_t id = 1;
	
	return Value_construct(id, this->phrases);
}
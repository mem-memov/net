#ifndef STORE_H
#define STORE_H

#include <stdio.h>
#include <stdlib.h>

struct Store;

struct Store * Store_construct(void * graph, void * lock);

void * Store_destruct
(struct Store * this);

char Store_hasFreePlace
(struct Store * this);

size_t Store_addNode
(struct Store * this);

void Store_removeNode
(struct Store * this, size_t place);

void Store_connectNodes
(struct Store * this, size_t origin, size_t destination);

void Store_disconnectNodes
(struct Store * this, size_t origin, size_t destination);

void Store_getNodeDestinations
(struct Store * this, size_t origin, size_t ** destinations, size_t * length);

void Store_getNodeOrigins
(struct Store * this, size_t destination, size_t ** origins, size_t * length);

char Store_isNode
(struct Store * this, size_t place);

void Store_export
(struct Store * this, FILE * file);

void Store_import
(struct Store * this, FILE * file);

#endif

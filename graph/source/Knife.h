#ifndef KNIFE_H
#define KNIFE_H

#include "Boat.h"
#include "Node.h"

struct Knife;

struct Knife * Knife_construct(struct Boat * boat);

void * Knife_destruct(struct Knife * this);

void Knife_cutDestination(struct Knife * this, struct Node * originNode, struct Node * destinationNode);

void Knife_cutOrigin(struct Knife * this, struct Node * originNode, struct Node * destinationNode);

#endif

#include "Object.h"

struct Object
{
	size_t id;
	struct Phrases * phrases;
};

struct Object * Object_construct(size_t id, struct Phrases * phrases)
{
	struct Object * this = malloc(sizeof(struct Object));

	this->id = id;
	this->phrases = phrases;

	return this;
}

void * Object_destruct(struct Object * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

#ifndef PARSERS_H
#define PARSERS_H

#include "Parser.h"

struct Parsers;

struct Parsers * Parsers_construct();

void * Parsers_destruct(struct Parsers * this);

struct Parser * Parsers_make(struct Parsers * this, char * string);

#endif

#include "Application.h"
#include "Error.h"
#include <netdatabase.h>
#include <stdlib.h>
#include <string.h>

struct Application {
	struct Database * database;
};

struct Application * Application_construct()
{
	struct Application * this = malloc(sizeof(struct Application));

	struct Databases * databases = Databases_construct();
	this->database = Databases_make(databases, 100, 4);
	databases = Databases_destruct(databases);

	return this;
}

void * Application_destruct(struct Application * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

char * Application_execute(struct Application * this, const char * const request)
{
	char * response = malloc(sizeof(char) * strlen("qq"));
	strcpy(response, "qq");
	return response;
}

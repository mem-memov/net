#ifndef WORD_H
#define WORD_H

#include <stdlib.h>
#include "Characters.h"

struct Word;

struct Word * Word_construct(size_t id, struct Characters * characters);

void * Word_destruct(struct Word * this);

#endif

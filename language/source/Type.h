#ifndef TYPE_H
#define TYPE_H

struct Type;

struct Type * Type_construct(
    size_t node, 
    struct Database * database, 
    struct Type * next
);

void * Type_destruct(struct Type * this);

#endif

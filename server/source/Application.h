#ifndef APPLICATION_HEADER
#define APPLICATION_HEADER

#include "Request.h"

struct Application;

struct Application * Application_construct();

void * Application_destruct(struct Application * this);

char * Application_execute(struct Application * this, const char * const request);

#endif

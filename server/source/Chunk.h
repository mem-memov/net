#ifndef CHUNK_H
#define CHUNK_H

struct Chunk;

struct Chunk * Chunk_construct(unsigned char * buffer, long int length, struct Chunk * previous);

void * Chunk_destruct(struct Chunk * this);

long int Chunk_getTotalLength(struct Chunk * this);

void Chunk_toString(struct Chunk * this, char * string);

char Chunk_endsWith(struct Chunk * this, char * end);

#endif

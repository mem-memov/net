#ifndef PARAMETER_H
#define PARAMETER_H

#include <stdlib.h>
#include "Phrase.h"

struct Parameter;

struct Parameter * Parameter_construct(size_t id, struct Phrases * phrases);

void * Parameter_destruct(struct Parameter * this);

#endif

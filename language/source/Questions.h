#ifndef QUESTIONS_H
#define QUESTIONS_H

#include "Database.h"
#include "Parser.h"
#include "Question.h"
#include "Triplets.h"

struct Questions;

struct Questions * Questions_construct(struct Triplets * triplets, struct Database * database);

void * Questions_destruct(struct Questions * this);

struct Question * Questions_make(struct Questions * this, struct Parser * parser);

#endif

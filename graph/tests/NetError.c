#include "../source/NetError.h"

#define NETERROR_MAX_CALLS 1

struct NetError
{
	char call;
	char * method[NETERROR_MAX_CALLS];
	void * destructed[NETERROR_MAX_CALLS];
	size_t nextPlace[NETERROR_MAX_CALLS];
	size_t graphSize[NETERROR_MAX_CALLS];
};

struct NetError * NetError_mock()
{
	struct NetError * this = malloc(sizeof(struct NetError));
	
	this->call = 0;
	
	char i;
	for (i = 0; i < NETERROR_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->nextPlace[i] = 55555;
		this->graphSize[i] = 55555;
	}
	
	return this;
}

void * NetError_unmock(struct NetError * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

void * NetError_destruct(struct NetError * this)
{
	this->method[this->call] = "NetError_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void NetError_requireFittingInSize(struct NetError * this, size_t nextPlace, size_t graphSize)
{
	this->method[this->call] = "NetError_requireFittingInSize";
	this->nextPlace[this->call] = nextPlace;
	this->graphSize[this->call] = graphSize;
	
	this->call++;
}
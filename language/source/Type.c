#include "Type.h"
#include <stdlib.h>
#include "Collection.h"

struct Type {
	size_t node;
	struct Database * database;
	struct Type * next;
};

struct Type * Type_construct(
	size_t node, 
	struct Database * database, 
	struct Type * next
) {
	struct Type * this = malloc(sizeof (struct Type));
	
	this->node = node;
	this->database = database;
	this->next = next;

	return this;
}

void * Type_destruct(struct Type * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void Type_occupyNodes(struct Type * this)
{
	struct Collection * collection = Database_createNode(this->database, NULL, NULL);
	
	if ( ! Collection_hasNode(collection, this->node) ) {
		exit(1);
	}
	
	if ( NULL != this->next ) {
		Type_occupyNodes(this->next);
	}
}

struct Collection * Type_collectDestinations(struct Type * this)
{
	return Database_readDestinationsOfNode(this->database, this->node);
}

struct Collection * Type_collectOrigins(struct Type * this)
{
	return Database_readOriginsOfNode(this->database, this->node);
}

size_t Type_createElbowNode(struct Type * this, struct Type * that)
{
	struct Collection * collection = Database_createNode(
		this->database, 
		Database_collectNode(this->database, this->node),
		Database_collectNode(this->database, this->node)
	);
	
	return Collection_getAt(collection, 0);
}
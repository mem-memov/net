#include "Objects.h"
#include <stdlib.h>
#include "WordParser.h"

struct Objects
{
	struct Phrases * phrases;
	struct Database * database;
};

struct Objects * Objects_construct(struct Phrases * phrases, struct Database * database)
{
	struct Objects * this = malloc(sizeof(struct Objects));

	this->phrases = phrases;
	
	this->database = database;
	
	return this;
}

void * Objects_destruct(struct Objects * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Object * Objects_make(struct Objects * this, struct Parser * parser)
{
	struct WordParser * wordParser = Parser_makeWordParser(parser, 1);
	
	struct Phrase * phrase = Phrases_make(this->phrases, wordParser);
	
	size_t id = 1;
	
	return Object_construct(id, this->phrases);
}
#ifndef CHARACTERS_H
#define CHARACTERS_H

#include "Database.h"
#include "Character.h"
#include "Numbers.h"

struct Characters;

struct Characters * Characters_construct(struct Database * database, struct Numbers * numbers);

void * Characters_destruct(struct Characters * this);

struct Character * Characters_make(struct Characters * this, char * numberString);

#endif

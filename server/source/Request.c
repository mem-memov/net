#include "Request.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Chunk.h"
#include "Error.h"

struct Request {
    int maxLength;
	struct Chunk * chunk;
	char * body;
};

struct Request * Request_construct(int maxLength)
{
	struct Request * this = malloc(sizeof(struct Request));

    Error_inRequestWhileConstructingWithMaxLength(maxLength);

	this->maxLength = maxLength;
	this->chunk = NULL;
	this->body = NULL;

	return this;
}

void * Request_destruct(struct Request * this)
{
	if ( NULL != this ) {
		this->chunk = Chunk_destruct(this->chunk);
		free(this);
	}

	return NULL;
}

const char * const Request_getBody(struct Request * this)
{
	long int length;
	
	if ( NULL == this->body ) {

		if ( NULL != this->chunk ) {
			length = Chunk_getTotalLength(this->chunk) + 1;
			this->body = malloc(sizeof(char) * length);
			Chunk_toString(this->chunk, this->body);
			this->body[length] = '\0';
		} else {
			length = strlen("");
			this->body = malloc(sizeof(char) * length);
			strcpy(this->body, "");
		}
		
	}

    return this->body;
}

char Request_isClean(struct Request * this)
{
	if ( NULL == this->body && NULL == this->chunk ) {
		return 1;
	}
	
	return 0;
}

void Request_clean(struct Request * this)
{
	this->chunk = Chunk_destruct(this->chunk);

	this->body = NULL;
}

void Request_appendChunk(struct Request * this, char * buffer, long int length)
{
	if ( NULL != this->body ) {
		exit(1);
	}
	
	if ( 0 == length ) {
		return;
	}
	
	if ( NULL != this->chunk && Chunk_getTotalLength(this->chunk) + length > this->maxLength ) {
		exit(1);
	}
	
	this->chunk = Chunk_construct(buffer, length, this->chunk);
}

char Request_isComplete(struct Request * this)
{
	if ( NULL != this->body ) {
		exit(1);
	}
	
	if ( NULL == this->chunk) {
		exit(1);
	}
	
	return Chunk_endsWith(this->chunk, "\r\n\r\n");
}
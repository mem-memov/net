#ifndef REQUEST_HEADER
#define REQUEST_HEADER

struct Request;

struct Request * Request_construct(int maxLength);

void * Request_destruct(struct Request * this);

const char * const Request_getBody(struct Request * this);

char Request_isClean(struct Request * this);

void Request_clean(struct Request * this);

void Request_appendChunk(struct Request * this, char * buffer, long int length);

char Request_isComplete(struct Request * this);

#endif

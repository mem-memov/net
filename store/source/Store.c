#include "Store.h"
#include "../../graph/source/Graph.h"
#include "../../graph/source/Graphs.h"
#include "Lock.h"

struct Store {
	struct Graph * graph;
	struct Lock * lock;
};

struct Store * Store_construct(void * graph, void * lock)
{
	struct Store * this = malloc(sizeof (struct Store));
	
	this->graph = (struct Graph *)graph;
	this->lock = (struct Lock *)lock;

	return this;
}

void * Store_destruct(struct Store * this)
{
	if (NULL != this) {
		this->graph = Graph_destruct(this->graph);
		this->lock = Lock_destruct(this->lock);
		free(this);
	}

	return NULL;
}

char Store_hasFreePlace(struct Store * this)
{
	Lock_holdGraph(this->lock);
	
	Graph_hasFreePlace(this->graph);
	
	Lock_releaseGraph(this->lock);
}

size_t Store_addNode(struct Store * this)
{
	return Graph_addNode(this->graph);
}

void Store_removeNode(struct Store * this, size_t place)
{
	Lock_holdRemovingNode(this->lock, place);
	
	Graph_removeNode(this->graph, place);
	
	Lock_releaseRemovingNode(this->lock, place);
}

void Store_connectNodes(struct Store * this, size_t origin, size_t destination)
{
	Lock_holdConnection(this->lock, origin, destination);
	
	Graph_connectNodes(this->graph, origin, destination);
	
	Lock_releaseConnection(this->lock, origin, destination);
}

void Store_disconnectNodes(struct Store * this, size_t origin, size_t destination)
{
	Lock_holdConnection(this->lock, origin, destination);
	
	Graph_disconnectNodes(this->graph, origin, destination);
	
	Lock_releaseConnection(this->lock, origin, destination);
}

void Store_getNodeDestinations(struct Store * this, size_t origin, size_t ** destinations, size_t * length)
{
	Lock_holdReadingNode(this->lock, origin);
	
	Graph_getNodeDestinations(this->graph, origin, destinations, length);
	
	Lock_releaseReadingNode(this->lock, origin);
}

void Store_getNodeOrigins(struct Store * this, size_t destination, size_t ** origins, size_t * length)
{
	Lock_holdReadingNode(this->lock, destination);
	
	Graph_getNodeOrigins(this->graph, destination, origins, length);
	
	Lock_releaseReadingNode(this->lock, destination);
}

char Store_isNode(struct Store * this, size_t place)
{
	Lock_holdReadingNode(this->lock, place);
	
	char isNode = Graph_isNode(this->graph, place);
	
	Lock_releaseReadingNode(this->lock, place);
	
	return isNode; // really safe ???
}

void Store_export(struct Store * this, FILE * file)
{
	Lock_holdGraph(this->lock);
	
	Graph_export(this->graph, file);
	
	Lock_releaseGraph(this->lock);
}

void Store_import(struct Store * this, FILE * file)
{
	Lock_holdGraph(this->lock);
	
	Graph_import(this->graph, file);
	
	Lock_releaseGraph(this->lock);
}
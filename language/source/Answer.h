#ifndef ANSWER_H
#define ANSWER_H

#include <stdlib.h>
#include "Triplets.h"

struct Answer;

struct Answer * Answer_construct(size_t id, struct Triplets * triplets);

void * Answer_destruct(struct Answer * this);

#endif

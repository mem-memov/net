#ifndef ANSWERS_H
#define ANSWERS_H

#include "Answer.h"
#include "Database.h"
#include "Parser.h"
#include "Triplets.h"

struct Answers;

struct Answers * Answers_construct(struct Triplets * triplets, struct Database * database);

void * Answers_destruct(struct Answers * this);

struct Answer * Answers_make(struct Answers * this, struct Parser * parser);

#endif

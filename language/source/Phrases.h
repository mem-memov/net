#ifndef PHRASES_H
#define PHRASES_H

#include "Database.h"
#include "Phrase.h"
#include "Words.h"
#include "WordParser.h"

struct Phrases;

struct Phrases * Phrases_construct(struct Words * words, struct Database * database);

void * Phrases_destruct(struct Phrases * this);

struct Phrase * Phrases_make(struct Phrases * this, struct WordParser * wordParser);

#endif

#include "Types.h"
#include <stdlib.h>

struct Types {
	char nodesOccupied;
	
	struct Type * zero;
	struct Type * one;
	struct Type * two;
	struct Type * three;
	struct Type * four;
	struct Type * five;
	struct Type * six;
	struct Type * seven;
	struct Type * eight;
	struct Type * nine;
	struct Type * message;
	struct Type * question;
	struct Type * answer; 
	struct Type * triplet; 
	struct Type * object; 
	struct Type * parameter; 
	struct Type * value; 
	struct Type * author;
	struct Type * phrase;
	struct Type * word;
	struct Type * character;
};

struct Types * Types_construct(struct Database * database)
{
	struct Types * this = malloc(sizeof (struct Types));
	
	this->nodesOccupied = 0;
	
	this->character = Type_construct(126, database, NULL);
	this->word = Type_construct(120, database, this->character);
	this->phrase = Type_construct(114, database, this->word);
	this->author = Type_construct(108, database, this->phrase);
	this->value = Type_construct(102, database, this->author);
	this->parameter = Type_construct(96, database, this->value);
	this->object = Type_construct(90, database, this->parameter);
	this->triplet = Type_construct(84, database, this->object);
	this->answer = Type_construct(78, database, this->triplet);
	this->question = Type_construct(72, database, this->answer);
	this->message = Type_construct(66, database, this->question);
	this->nine = Type_construct(60, database, this->message);
	this->eight = Type_construct(54, database, this->nine);
	this->seven = Type_construct(48, database, this->eight);
	this->six = Type_construct(42, database, this->seven);
	this->five = Type_construct(36, database, this->six);
	this->four = Type_construct(30, database, this->five);
	this->three = Type_construct(24, database, this->four);
	this->two = Type_construct(18, database, this->three);
	this->one = Type_construct(12, database, this->two);
	this->zero = Type_construct(6, database, this->one);

	return this;
}

void * Types_destruct(struct Types * this)
{
	if (NULL != this) {
		this->character = Type_destruct(this->character);
		this->word = Type_destruct(this->word);
		this->phrase = Type_destruct(this->phrase);
		this->author = Type_destruct(this->author);
		this->value = Type_destruct(this->value);
		this->parameter = Type_destruct(this->parameter);
		this->object = Type_destruct(this->object);
		this->triplet = Type_destruct(this->triplet);
		this->answer = Type_destruct(this->answer);
		this->question = Type_destruct(this->question);
		this->message = Type_destruct(this->message);
		this->nine = Type_destruct(this->nine);
		this->eight = Type_destruct(this->eight);
		this->seven = Type_destruct(this->seven);
		this->six = Type_destruct(this->six);
		this->five = Type_destruct(this->five);
		this->four = Type_destruct(this->four);
		this->three = Type_destruct(this->three);
		this->two = Type_destruct(this->two);
		this->one = Type_destruct(this->one);
		this->zero = Type_destruct(this->zero);
		free(this);
	}

	return NULL;
}

void Types_occupyNodes(struct Types * this)
{
	if (this->nodesOccupied) {
		return;
	}
	
	Type_occupyNodes(this->zero);
	
	this->nodesOccupied = 1;
}

struct Type * Types_makeZero(struct Types * this)
{
	return this->zero;
}

struct Type * Types_makeOne(struct Types * this)
{
	return this->one;
}

struct Type * Types_makeTwo(struct Types * this)
{
	return this->two;
}

struct Type * Types_makeThree(struct Types * this)
{
	return this->three;
}

struct Type * Types_makeFour(struct Types * this)
{
	return this->four;
}

struct Type * Types_makeFive(struct Types * this)
{
	return this->five;
}

struct Type * Types_makeSix(struct Types * this)
{
	return this->six;
}

struct Type * Types_makeSeven(struct Types * this)
{
	return this->seven;
}

struct Type * Types_makeEight(struct Types * this)
{
	return this->eight;
}

struct Type * Types_makeNine(struct Types * this)
{
	return this->nine;
}

struct Type * Types_makeMessage(struct Types * this)
{
	return this->message;
}

struct Type * Types_makeQuestion(struct Types * this)
{
	return this->question;
}

struct Type * Types_makeTriplet(struct Types * this)
{
	return this->triplet;
}

struct Type * Types_makeObject(struct Types * this)
{
	return this->object;
}

struct Type * Types_makeParameter(struct Types * this)
{
	return this->parameter;
}

struct Type * Types_makeAuthor(struct Types * this)
{
	return this->author;
}

struct Type * Types_makePhrase(struct Types * this)
{
	return this->phrase;
}

struct Type * Types_makeWord(struct Types * this)
{
	return this->word;
}

struct Type * Types_makeCharacter(struct Types * this)
{
	return this->character;
}
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Database.h"
#include "Databases.h"
#include "Language.h"
#include "Languages.h"

static void convertCharactersToNumbers(const char * input, char * convertedInput)
{
	convertedInput[0] = '\0';
	
	int code;
	char codeString[10];
	int in;
	for (in=0; in<strlen(input); in++) {
		if (input[in] == ':') {
			if (convertedInput[strlen(convertedInput)-1] == '_') {
				convertedInput[strlen(convertedInput)-1] = '\0';
			}
			strcat(convertedInput, ":");
			continue;
		}
		if (input[in] == ' ') {
			if (convertedInput[strlen(convertedInput)-1] == '_') {
				convertedInput[strlen(convertedInput)-1] = '\0';
			}
			if (in > 0 && input[in-1] != ' ') {
				strcat(convertedInput, " ");
			}
			continue;
		}
		code = input[in];
		sprintf(codeString, "%d", code);
		strcat(convertedInput, codeString);
		strcat(convertedInput, "_");
	}
	
	if (convertedInput[strlen(convertedInput)-1] == '_') {
		convertedInput[strlen(convertedInput)-1] = '\0';
	}
}

int main(int argc, char ** argv)
{
	char input[100];
	char convertedInput[400];
	char * output;

	struct Databases * databases = Databases_construct();
	size_t graphSize = 1000;
	size_t placeSize = 4;
	struct Database * database = Databases_make(databases, graphSize, placeSize);
	struct Languages * languages = Languages_construct(database);
	struct Language * language = Languages_make(languages);

	while (1) {
		printf("User: ");
		scanf(" %[^\n]s", input); //spaces included & new line handled

		if (strcmp("exit", input) == 0 || strcmp("q", input) == 0) {
			
			break;
		}

		convertCharactersToNumbers(input, convertedInput);
		printf("Converted: %s\n", convertedInput);
		
		Language_interpret(language, convertedInput, &output);
	
	}
	
	return (EXIT_SUCCESS);
}



#include "Characters.h"
#include <stdlib.h>
#include <string.h>

struct Characters
{
	struct Database * database;
	struct Numbers * numbers;
};

struct Characters * Characters_construct(struct Database * database, struct Numbers * numbers)
{
	struct Characters * this = malloc(sizeof(struct Characters));
	
	this->database = database;
	this->numbers = numbers;

	return this;
}

void * Characters_destruct(struct Characters * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Character * Characters_make(struct Characters * this, char * numberString)
{
	size_t length = strlen(numberString);
	
	struct Number * previous = NULL;
	struct Number * current = NULL;
	size_t elbowNode;
	
	size_t i = 0;
	for ( i = 0; i < length; i++ ) {
		
		current = Numbers_make(this->numbers, numberString[i]);
		
		if ( 0 == i ) {
			continue;
		}
		
		elbowNode = Number_getElbowNode(previous, current);
		
		// TODO: connect character numbers

	}

	
	size_t id = 1;
	
	return Character_construct(id, this->numbers);
}
#ifndef AUTHORS_H
#define AUTHORS_H

#include "Author.h"
#include "Database.h"
#include "Parser.h"
#include "Phrases.h"

struct Authors;

struct Authors * Authors_construct(struct Phrases * phrases, struct Database * database);

void * Authors_destruct(struct Authors * this);

struct Author * Authors_make(struct Authors * this, struct Parser * parser);

#endif

#include "../source/Boat.h"

#define BOAT_MAX_CALLS 2

struct Boat {
	char call;
	char * method[BOAT_MAX_CALLS];
	void * destructed[BOAT_MAX_CALLS];
	size_t place[BOAT_MAX_CALLS];
	char isCovering[BOAT_MAX_CALLS];
	char hasSpaceForEntry[BOAT_MAX_CALLS];
	size_t linkPlace[BOAT_MAX_CALLS];
	size_t nodePlace[BOAT_MAX_CALLS];
	struct Stream * stream[BOAT_MAX_CALLS];
	struct Export * export[BOAT_MAX_CALLS];
};

struct Boat * Boat_mock()
{
	struct Boat * this = malloc(sizeof(struct Boat));

	this->call = 0;
	
	char i;
	for (i = 0; i < BOAT_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->place[i] = 55555;
		this->isCovering[i] = 47;
		this->hasSpaceForEntry[i] = 48;
		this->linkPlace[i] = 55555;
		this->nodePlace[i] = 55555;
		this->stream[i] = NULL;
		this->export[i] = NULL;
	}

	return this;
}

void * Boat_unmock(struct Boat * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

void * Boat_destruct(struct Boat * this)
{
	this->method[this->call] = "Boat_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void Boat_create(struct Boat * this)
{
	this->method[this->call] = "Boat_create";
	
	this->call++;
}

void Boat_read(struct Boat * this)
{
	this->method[this->call] = "Boat_read";
	
	this->call++;
}

char Boat_isCovering(struct Boat * this, size_t place)
{
	this->method[this->call] = "Boat_isCovering";
	this->place[this->call] = place;
	
	char isCovering = this->isCovering[this->call];
	
	this->call++;
	
	return isCovering;
}

char Boat_hasSpaceForEntry(struct Boat * this)
{
	this->method[this->call] = "Boat_hasSpaceForEntry";
	
	char hasSpaceForEntry = this->hasSpaceForEntry[this->call];
	
	this->call++;
	
	return hasSpaceForEntry;
}

size_t Boat_createLinkEntry(struct Boat * this)
{
	this->method[this->call] = "Boat_createLinkEntry";
	
	char linkPlace = this->linkPlace[this->call];
	
	this->call++;
	
	return linkPlace;
}

size_t Boat_createNodeEntry(struct Boat * this)
{
	this->method[this->call] = "Boat_createNodeEntry";
	
	char nodePlace = this->nodePlace[this->call];
	
	this->call++;
	
	return nodePlace;
}

void Boat_deleteLinkEntry(struct Boat * this, size_t place)
{
	this->method[this->call] = "Boat_deleteLinkEntry";
	this->place[this->call] = place;

	this->call++;
}

void Boat_deleteNodeEntry(struct Boat * this, size_t place)
{
	this->method[this->call] = "Boat_deleteNodeEntry";
	this->place[this->call] = place;

	this->call++;
}

struct Export * Boat_createExport(struct Boat * this)
{
	this->method[this->call] = "Boat_createExport";
	
	struct Export * export = this->export[this->call];
	
	this->call++;
	
	return export;
}

void Boat_import(struct Boat * this, struct Stream * stream)
{
	this->method[this->call] = "Boat_import";
	this->stream[this->call] = stream;

	this->call++;
}
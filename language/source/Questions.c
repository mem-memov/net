#include "Questions.h"
#include <stdlib.h>

struct Questions
{
	struct Triplets * triplets;
	struct Database * database;
};

struct Questions * Questions_construct(struct Triplets * triplets, struct Database * database)
{
	struct Questions * this = malloc(sizeof(struct Questions));
	
	this->triplets = triplets;
	
	this->database = database;

	return this;
}

void * Questions_destruct(struct Questions * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Question * Questions_make(struct Questions * this, struct Parser * parser)
{
	struct Triplet * triplet = Triplets_make(this->triplets, parser);
	
	size_t id = 1;
	
	return Question_construct(id, this->triplets);
}
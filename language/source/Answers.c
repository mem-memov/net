#include "Answers.h"
#include <stdlib.h>
#include "Triplet.h"

struct Answers
{
	struct Triplets * triplets;
	struct Database * database;
};

struct Answers * Answers_construct(struct Triplets * triplets, struct Database * database)
{
	struct Answers * this = malloc(sizeof(struct Answers));
	
	this->triplets = triplets;
	
	this->database = database;

	return this;
}

void * Answers_destruct(struct Answers * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Answer * Answers_make(struct Answers * this, struct Parser * parser)
{
	struct Triplet * triplet = Triplets_make(this->triplets, parser);
	
	size_t id = 1;
	
	return Answer_construct(id, this->triplets);
}

#ifndef NUMBERS_H
#define NUMBERS_H

#include "Number.h"

struct Numbers;

struct Numbers * Numbers_construct();

void * Numbers_destruct(struct Numbers * this);

struct Number * Numbers_make(struct Numbers * this, char number);

#endif

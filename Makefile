app: server

server: language
	cd server/source && $(MAKE)
	
language: database
	cd language/source && $(MAKE)
	
database: store
	cd database/source && $(MAKE)
	
store: graph
	cd store/source && $(MAKE)
	
graph:
	cd graph/source && $(MAKE)

	
test: netgraph-test netstore-test netdatabase-test netserver-test

netserver-test:
	cd server/tests && $(MAKE)
	
netdatabase-test:
	cd database/tests && $(MAKE)
	
netstore-test:
	cd store/tests && $(MAKE)
	
netgraph-test:
	cd graph/tests && $(MAKE)
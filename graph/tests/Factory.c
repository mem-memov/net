#include "../source/Factory.h"
#include <stdlib.h>

#define FACTORY_MAX_CALLS 10

struct Factory {
	char call;
	char * method[FACTORY_MAX_CALLS];
	void * destructed[FACTORY_MAX_CALLS];
	struct Boat * boat[FACTORY_MAX_CALLS];
	struct Imports * imports[FACTORY_MAX_CALLS];
	struct Link * link[FACTORY_MAX_CALLS];
	struct Node * node[FACTORY_MAX_CALLS];
	struct Knife * knife[FACTORY_MAX_CALLS];
};

struct Factory * Factory_mock()
{
	struct Factory * this = malloc(sizeof (struct Factory));

	this->call = 0;
	
	char i;
	for (i = 0; i < FACTORY_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->boat[i] = NULL;
		this->imports[i] = NULL;
		this->link[i] = NULL;
		this->node[i] = NULL;
		this->knife[i] = NULL;
	}
	
	return this;
}

void * Factory_unmock(struct Factory * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void * Factory_destruct(struct Factory * this)
{
	this->method[this->call] = "Factory_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}


struct Boat * Factory_makeBoat(struct Factory * this)
{
	this->method[this->call] = "Factory_makeBoat";
	
	struct Boat * boat = this->boat[this->call];
	
	this->call++;
	
	return boat;
}

struct Imports * Factory_makeImports(struct Factory * this)
{
	this->method[this->call] = "Factory_makeImports";
	
	struct Imports * imports = this->imports[this->call];
	
	this->call++;
	
	return imports;
}

struct Link * Factory_makeLink(struct Factory * this)
{
	this->method[this->call] = "Factory_makeLink";
	
	struct Link * link = this->link[this->call];
	
	this->call++;
	
	return link;
}

struct Node * Factory_makeNode(struct Factory * this)
{
	this->method[this->call] = "Factory_makeNode";
	
	struct Node * node = this->node[this->call];
	
	this->call++;
	
	return node;
}

struct Knife * Factory_makeKnife(struct Factory * this, struct Boat * boat)
{
	this->method[this->call] = "Factory_makeKnife";
	this->boat[this->call] = boat;
	
	struct Knife * knife = this->knife[this->call];
	
	this->call++;
	
	return knife;
}
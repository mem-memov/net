#ifndef TRIPLETS_H
#define TRIPLETS_H

#include "Database.h"
#include "Objects.h"
#include "Parameters.h"
#include "Parser.h"
#include "Triplet.h"
#include "Values.h"

struct Triplets;

struct Triplets * Triplets_construct(
    struct Objects * objects, 
    struct Parameters * parameters, 
    struct Values * values, 
    struct Database * database
);

void * Triplets_destruct(struct Triplets * this);

struct Triplet * Triplets_make(struct Triplets * this, struct Parser * parser);

#endif

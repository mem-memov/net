#include "Intersector.h"

size_t Intersector_count(size_t * theseItems, size_t thisLength, size_t * thoseItems, size_t thatLength)
{
	size_t thisOffset = 0;
	size_t thatOffset = 0;
	size_t resultOffset = 0;

	while ((thisOffset < thisLength) && (thatOffset < thatLength))
	{
		if (theseItems[thisOffset] == thoseItems[thatOffset])
		{
			thisOffset++;
            thatOffset++;
            resultOffset++;
		}
		else
		{
			if (theseItems[thisOffset] < thoseItems[thatOffset])
			{
				thisOffset++;
			}
			else
			{
				thatOffset++;
			}
		}
	}

	return resultOffset;
}

void Intersector_intersect(size_t * theseItems, size_t thisLength, size_t * thoseItems, size_t thatLength, size_t * resultItems, size_t resultLength)
{
	size_t thisOffset = 0;
	size_t thatOffset = 0;
	size_t resultOffset = 0;

	while ((thisOffset < thisLength) && (thatOffset < thatLength))
	{
		if (theseItems[thisOffset] == thoseItems[thatOffset])
		{
			resultItems[resultOffset] = theseItems[thisOffset];
			thisOffset++;
            thatOffset++;
            resultOffset++;
		}
		else
		{
			if (theseItems[thisOffset] < thoseItems[thatOffset])
			{
				thisOffset++;
			}
			else
			{
				thatOffset++;
			}
		}
	}
}

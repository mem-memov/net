#ifndef CONNECTION_HEADER
#define CONNECTION_HEADER

#include "Application.h"
#include "Request.h"

struct Connection;

struct Connection * Connection_construct(int descriptor, struct Request * request);

void * Connection_destruct(struct Connection * this);

void Connection_close(struct Connection * this);

void Connection_receive(struct Connection * this);

void Connection_apply(struct Connection * this, struct Application * application);

void Connection_send(struct Connection * this);
#endif

#ifndef LOCKS_H
#define LOCKS_H

#include "Lock.h"

struct Locks;

struct Locks * Locks_construct();

void * Locks_destruct(struct Locks * this);

struct Lock * Locks_make(struct Locks * this);

#endif

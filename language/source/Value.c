#include "Value.h"

struct Value
{
	size_t id;
	struct Phrases * phrases;
};

struct Value * Value_construct(size_t id, struct Phrases * phrases)
{
	struct Value * this = malloc(sizeof(struct Value));

	this->id = id;
	this->phrases = phrases;
	
	return this;
}

void * Value_destruct(struct Value * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

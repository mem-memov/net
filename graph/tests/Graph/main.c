#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../../source/Graph.c"
#include "../Boat.c"
#include "../Export.c"
#include "../Factory.c"
#include "../Import.c"
#include "../Imports.c"
#include "../Knife.c"
#include "../Link.c"
#include "../Node.c"

struct Graph * graph;
unsigned char * bytes;
struct Node * node;
struct Node * originNode;
struct Node * destinationNode;
struct Link * link;
struct Boat * boat;
struct Knife * knife;
struct Imports * imports;

void prepareTest()
{
	size_t graphSize = 600;
	size_t entrySize = 6;
	size_t placeSize = 5;
	unsigned char * bytes = malloc(graphSize * entrySize * placeSize);
	
	node = Node_mock();
	originNode = Node_mock();
	destinationNode = Node_mock();
	link = Link_mock();
	boat = Boat_mock();
	knife = Knife_mock();
	imports = Imports_mock();
	
	graph = Graph_construct(
		bytes,
		node,
		originNode,
		destinationNode,
		link,
		boat,
		knife,
		imports
	);
}

void demolishTest()
{
	Graph_destruct(graph);

	node = Node_unmock(node);
	originNode = Node_unmock(originNode);
	destinationNode = Node_unmock(destinationNode);
	link = Link_unmock(link);
	boat = Boat_unmock(boat);
	knife = Knife_unmock(knife);
	imports = Imports_unmock(imports);
}

void it_checks_free_space()
{
	prepareTest();
	
	boat->hasSpaceForEntry[0] = 1;
	
	char result = Graph_hasFreePlace(graph);
	
	assert(0 == strcmp(boat->method[0], "Boat_hasSpaceForEntry"));	
	
	assert(result == 1);

	demolishTest();
}

void it_adds_node()
{
	prepareTest();
	
	size_t nodePlace = 36;
	
	boat->nodePlace[0] = nodePlace;
	
	size_t result = Graph_addNode(graph);
	
	assert(0 == strcmp(boat->method[0], "Boat_createNodeEntry"));
	
	assert(
		0 == strcmp(node->method[0], "Node_create") 
		&& node->place[0] == nodePlace
	);
	
	assert(result == nodePlace);
	
	demolishTest();
}

void it_removes_node()
{
	prepareTest();
	
	size_t nodePlace = 36;
	
	Graph_removeNode(graph, nodePlace);
	
	assert(
		0 == strcmp(node->method[0], "Node_read") 
		&& node->place[0] == nodePlace
	);
	
	assert(0 == strcmp(node->method[1], "Node_delete"));
	
	assert(
		0 == strcmp(boat->method[0], "Boat_deleteNodeEntry") 
		&& boat->place[0] == nodePlace
	);
	
	demolishTest();
}

void it_connects_nodes()
{
	prepareTest();
	
	size_t origin = 72;
	size_t destination = 18;
	size_t linkPlace = 36;
	
	boat->linkPlace[0] = linkPlace;
	
	Graph_connectNodes(graph, origin, destination);
	
	assert(
		0 == strcmp(originNode->method[0], "Node_read") 
		&& originNode->place[0] == origin
	);
	
	assert(
		0 == strcmp(destinationNode->method[0], "Node_read") 
		&& destinationNode->place[0] == destination
	);
	
	assert(0 == strcmp(boat->method[0], "Boat_createLinkEntry"));
	
	assert(
		0 == strcmp(link->method[0], "Link_create") 
		&& link->originNode[0] == origin
		&& link->destinationNode[0] == destination
	);
	
	assert(
		0 == strcmp(originNode->method[1], "Node_addOutgoingLink") 
		&& originNode->link[1] == link
	);
	
	assert(
		0 == strcmp(destinationNode->method[1], "Node_addIncomingLink") 
		&& destinationNode->link[1] == link
	);
	
	demolishTest();
}

void it_disconnects_destination_from_origin()
{
	prepareTest();
	
	size_t origin = 72;
	size_t destination = 18;
	
	originNode->isSmallOrigin[1] = 1;
	
	Graph_disconnectNodes(graph, origin, destination);
	
	assert(
		0 == strcmp(originNode->method[0], "Node_read") 
		&& originNode->place[0] == origin
	);
	
	assert(
		0 == strcmp(destinationNode->method[0], "Node_read") 
		&& destinationNode->place[0] == destination
	);
	
	assert(
		0 == strcmp(originNode->method[1], "Node_isSmallOrigin") 
		&& originNode->destination[1] == destinationNode
	);
	
	assert(
		0 == strcmp(knife->method[0], "Knife_cutDestination") 
		&& knife->originNode[0] == originNode
		&& knife->destinationNode[0] == destinationNode
	);
	
	demolishTest();
}

void it_disconnects_origin_from_destination()
{
	prepareTest();
	
	size_t origin = 72;
	size_t destination = 18;
	
	originNode->isSmallOrigin[1] = 0;
	
	Graph_disconnectNodes(graph, origin, destination);
	
	assert(
		0 == strcmp(originNode->method[0], "Node_read") 
		&& originNode->place[0] == origin
	);
	
	assert(
		0 == strcmp(destinationNode->method[0], "Node_read") 
		&& destinationNode->place[0] == destination
	);
	
	assert(
		0 == strcmp(originNode->method[1], "Node_isSmallOrigin") 
		&& originNode->destination[1] == destinationNode
	);
	
	assert(
		0 == strcmp(knife->method[0], "Knife_cutOrigin") 
		&& knife->originNode[0] == originNode
		&& knife->destinationNode[0] == destinationNode
	);
	
	demolishTest();
}

void it_supplies_node_destinations()
{
	prepareTest();
	
	size_t origin = 78;
	size_t * destinations;
	size_t length;
	
	Graph_getNodeDestinations(graph, origin, &destinations, &length);
	
	assert(
		0 == strcmp(node->method[0], "Node_read") 
		&& node->place[0] == origin
	);
	
	assert(
		0 == strcmp(node->method[1], "Node_getNodeDestinations") 
		&& node->destinations[1] == &destinations
		&& node->length[1] == &length
	);
	
	demolishTest();
}

void it_supplies_node_origins()
{
	prepareTest();
	
	size_t destination = 48;
	size_t * origins;
	size_t length;
	
	Graph_getNodeOrigins(graph, destination, &origins, &length);
	
	assert(
		0 == strcmp(node->method[0], "Node_read") 
		&& node->place[0] == destination
	);
	
	assert(
		0 == strcmp(node->method[1], "Node_getNodeOrigins") 
		&& node->origins[1] == &origins
		&& node->length[1] == &length
	);
	
	demolishTest();
}

void it_checks_node_place_that_is_too_large()
{
	prepareTest();
	
	size_t place = 36;
	
	boat->isCovering[0] = 0;
	
	char result = Graph_isNode(graph, place);
	
	assert(
		0 == strcmp(boat->method[0], "Boat_isCovering") 
		&& boat->place[0] == place
	);
	
	assert(result == 0);
	
	demolishTest();
}

void it_checks_node_place_that_is_unequal_itself()
{
	prepareTest();
	
	size_t place = 36;
	
	boat->isCovering[0] = 1;
	
	node->isNode[1] = 0;
	
	char result = Graph_isNode(graph, place);
	
	assert(
		0 == strcmp(boat->method[0], "Boat_isCovering") 
		&& boat->place[0] == place
	);
	
	assert(
		0 == strcmp(node->method[0], "Node_read") 
		&& node->place[0] == place
	);
	
	assert(0 == strcmp(node->method[1], "Node_isNode"));
	
	assert(result == 0);
	
	demolishTest();
}

void it_exports_data_to_stream()
{
	prepareTest();
	
	FILE * file; //uninitialized
	struct Export * export = Export_mock();
	
	boat->export[0] = export;
	
	Graph_export(graph, file);
	
	assert(0 == strcmp(boat->method[0], "Boat_createExport"));
	
	assert(
		0 == strcmp(export->method[0], "Export_write") 
		&& export->file[0] == file
	);
	
	assert(0 == strcmp(export->method[1], "Export_destruct"));
	
	demolishTest();
}

void it_imports_data_from_stream()
{
	prepareTest();
	
	FILE * file; //uninitialized
	struct Import * import = Import_mock();
	
	imports->import[0] = import;
	
	Graph_import(graph, file);
	
	assert(0 == strcmp(imports->method[0], "Imports_make"));
	
	assert(
		0 == strcmp(import->method[0], "Import_read") 
		&& import->file[0] == file
	);
	
	assert(0 == strcmp(import->method[1], "Import_destruct"));
	
	demolishTest();
}

int main(int argc, char** argv)
{
	it_checks_free_space();
	it_adds_node();
	it_removes_node();
	it_connects_nodes();
	it_disconnects_destination_from_origin();
	it_disconnects_origin_from_destination();
	it_supplies_node_destinations();
	it_supplies_node_origins();
	it_checks_node_place_that_is_too_large();
	it_checks_node_place_that_is_unequal_itself();
	it_exports_data_to_stream();
	it_imports_data_from_stream();
	
	return (EXIT_SUCCESS);
}

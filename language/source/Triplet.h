#ifndef TRIPLET_H
#define TRIPLET_H

#include <stdlib.h>
#include "Objects.h"
#include "Parameters.h"
#include "Values.h"

struct Triplet;

struct Triplet * Triplet_construct(
    size_t id, 
    struct Objects * objects, 
    struct Parameters * parameters, 
    struct Values * values
);

void * Triplet_destruct(struct Triplet * this);

#endif

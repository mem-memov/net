#ifndef PARAMETERS_H
#define PARAMETERS_H

#include "Database.h"
#include "Parser.h"
#include "Phrases.h"
#include "Parameter.h"

struct Parameters;

struct Parameters * Parameters_construct(struct Phrases * phrases, struct Database * database);

void * Parameters_destruct(struct Parameters * this);

struct Parameter * Parameters_make(struct Parameters * this, struct Parser * parser);

#endif

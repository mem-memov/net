#include "Messages.h"
#include <stdlib.h>

struct Messages
{
	struct Authors * authors;
	struct Answers * answers;
	struct Questions * questions;
	struct Database * database;
};

struct Messages * Messages_construct(
	struct Authors * authors, 
	struct Answers * answers, 
	struct Questions * questions, 
	struct Database * database
) {
	struct Messages * this = malloc(sizeof(struct Messages));
	
	this->authors = authors;
	this->answers = answers;
	this->questions = questions;
	
	this->database = database;

	return this;
}

void * Messages_destruct(struct Messages * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Message * Messages_make(struct Messages * this, struct Parser * parser)
{
	struct Author * author = Authors_make(this->authors, parser);
	
	struct Message * message;
	
	if ( Parser_isQuestion(parser) ) {
		struct Question * question = Questions_make(this->questions, parser);
		message = Message_constructQuestion(author, question);
	} else {
		struct Answer * answer = Answers_make(this->answers, parser);
		message = Message_constructAnswer(author, answer);
	}

	return message;
}

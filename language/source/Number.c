#include "Number.h"
#include <stdlib.h>
#include "Collection.h"

struct Number {
	char character;
	struct Type * type;
};

struct Number * Number_construct(char character, struct Type * type)
{
	struct Number * this = malloc(sizeof (struct Number));

	this->character = character;
	this->type = type;
	
	return this;
}

void * Number_destruct(struct Number * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

size_t Number_getElbowNode(struct Number * this, struct Number * that)
{
	struct Collection * thisDestinations = Type_collectDestinations(this->type);
	struct Collection * thatOrigins = Type_collectOrigins(that->type);
	
	struct Collection * intersection = Collection_intersect(thisDestinations, thatOrigins);
	
	size_t length = Collection_getLength(intersection);
	
	if ( 0 == length ) {
		return Type_createElbowNode(this->type, that->type);
	}
	
	if ( length > 1 ) {
		exit(1);
	}
	
	return Collection_getAt(intersection, 0);
}
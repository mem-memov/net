#include "Parameters.h"
#include <stdlib.h>
#include "WordParser.h"

struct Parameters
{
	struct Phrases * phrases;
	struct Database * database;
};

struct Parameters * Parameters_construct(struct Phrases * phrases, struct Database * database)
{
	struct Parameters * this = malloc(sizeof(struct Parameters));

	this->phrases = phrases;
	
	this->database = database;
	
	return this;
}

void * Parameters_destruct(struct Parameters * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Parameter * Parameters_make(struct Parameters * this, struct Parser * parser)
{
	struct WordParser * wordParser = Parser_makeWordParser(parser, 2);
	
	struct Phrase * phrase = Phrases_make(this->phrases, wordParser);
	
	size_t id = 1;
	
	return Parameter_construct(id, this->phrases);
}

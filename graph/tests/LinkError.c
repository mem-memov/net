#include "../source/LinkError.h"
#include <stdlib.h>

#define LINKERROR_MAX_CALLS 1

struct LinkError {
	char call;
	char * method[LINKERROR_MAX_CALLS];
	void * destructed[LINKERROR_MAX_CALLS];
	size_t origin[LINKERROR_MAX_CALLS];
	size_t destination[LINKERROR_MAX_CALLS];
};

struct LinkError * LinkError_mock()
{
	struct LinkError * this = malloc(sizeof(struct LinkError));
	
	this->call = 0;
	
	char i;
	for (i = 0; i < LINKERROR_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->origin[i] = 5555;
		this->destination[i] = 5555;
	}

	return this;
}

void * LinkError_unmock(struct LinkError * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

void * LinkError_destruct(struct LinkError * this)
{
	this->method[this->call] = "LinkError_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void LinkError_forbidSelfPointingNodes(struct LinkError * this, size_t origin, size_t destination)
{
	this->method[this->call] = "LinkError_forbidSelfPointingNodes";
	this->origin[this->call] = origin;
	this->destination[this->call] = destination;
	
	this->call++;
}

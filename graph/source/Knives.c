#include "Knives.h"
#include <stdlib.h>

struct Knives
{
	//
};

struct Knives * Knives_construct()
{
	struct Knives * this = malloc(sizeof(struct Knives));

	return this;
}

void * Knives_destruct(struct Knives * this)
{
	if (NULL == this) {
		return NULL;
	}

	free(this);

	return NULL;
}

struct Knife * Knives_make(struct Knives * this, struct Boat * boat)
{
	return Knife_construct(boat);
}

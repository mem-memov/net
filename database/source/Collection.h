#ifndef COLLECTION_H
#define COLLECTION_H

#include <stdlib.h>

struct Collection;

struct Collection * Collection_construct(size_t * nodes, size_t length);

void * Collection_destruct(struct Collection * this);

size_t Collection_getLength(struct Collection * this);

void Collection_addNode(struct Collection * this, size_t node);

char Collection_hasNode
(struct Collection * this, size_t node);

size_t Collection_getAt
(struct Collection * this, size_t index);

struct Collection * Collection_intersect
(struct Collection * this, struct Collection * that);

#endif
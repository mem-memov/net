#ifndef GRAPHS_H
#define GRAPHS_H

// Graph.h and Graphs.h get concatenated in Makefile
#ifndef GRAPH_H
#include "Graph.h"
#endif

struct Graphs;

struct Graphs * Graphs_construct
();

void * Graphs_destruct
(struct Graphs * this);

struct Graph * Graphs_make
(struct Graphs * this, size_t graphSize, size_t placeSize);

#endif

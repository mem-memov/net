#include "Message.h"
#include <stdlib.h>

struct Message
{
	struct Author * author;
	struct Answer * answer;
	struct Question * question;
};

static struct Message * Message_construct(struct Author * author)
{
	struct Message * this = malloc(sizeof(struct Message));
	
	this->author = author;
	this->answer = NULL;
	this->question = NULL;

	return this;
}

struct Message * Message_constructAnswer(struct Author * author, struct Answer * answer)
{
	struct Message * this = Message_construct(author);
	
	this->answer = answer;
	
	return this;
}

struct Message * Message_constructQuestion(struct Author * author, struct Question * question)
{
	struct Message * this = Message_construct(author);
	
	this->question = question;
	
	return this;
}

void * Message_destruct(struct Message * this)
{
	if (NULL != this) {
		this->author = Author_destruct(this->author);
		this->answer = Answer_destruct(this->answer);
		this->question = Question_destruct(this->question);
		free(this);
	}

	return NULL;
}

void Message_write(struct Message * this)
{
	//Author_write(this->author);
	
	if ( NULL != this->answer ) {
		//Answer_write(this->answer);
	}
	
	if ( NULL != this->question ) {
		//Question_write(this->question);
	}
}

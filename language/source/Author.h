#ifndef AUTHOR_H
#define AUTHOR_H

#include <stdlib.h>
#include "Phrases.h"

struct Author;

struct Author * Author_construct(size_t id, struct Phrase * phrases);

void * Author_destruct(struct Author * this);

#endif

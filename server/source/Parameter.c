#include "Parameter.h"
#include <stdlib.h>

struct Parameter
{
	struct Listener * listener;
	struct Application * application;
};

struct Parameter * Parameter_construct(
    struct Listener * listener, 
    struct Application * application
) {
	struct Parameter * this = malloc(sizeof(struct Parameter));
	
	this->listener = listener;
	this->application = application;

	return this;
}

void * Parameter_destruct(struct Parameter * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

struct Listener * Parameter_getListener(struct Parameter * this)
{
	return this->listener;
}

struct Application * Parameter_getApplication(struct Parameter * this)
{
	return this->application;
}
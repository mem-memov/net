#include "Words.h"
#include <stdlib.h>

struct Words
{
	struct Characters * characters;
	struct Database * database;
};

struct Words * Words_construct(struct Characters * characters, struct Database * database)
{
	struct Words * this = malloc(sizeof(struct Words));
	
	this->characters = characters;
	
	this->database = database;

	return this;
}

void * Words_destruct(struct Words * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Word * Words_make(struct Words * this, struct CharacterParser * caracterParser)
{
	struct Character ** characters;
	int length;
	
	CharacterParser_makeCharacters(caracterParser, &characters, &length, this->characters);
	
	size_t id = 1;
	
	return Word_construct(id, this->characters);
}

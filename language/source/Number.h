#ifndef NUMBER_H
#define NUMBER_H

#include "Type.h"

struct Number;

struct Number * Number_construct(char character, struct Type * type);

void * Number_destruct(struct Number * this);

#endif

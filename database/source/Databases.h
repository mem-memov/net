#ifndef DATABASES_H
#define DATABASES_H

#include <stdlib.h>

// Database.h and Databases.h get concatenated in Makefile
#ifndef DATABASE_H
#include "Database.h"
#endif

struct Databases;

struct Databases * Databases_construct
();

void * Databases_destruct
(struct Databases * this);

struct Database * Databases_make
(struct Databases * this, size_t graphSize, size_t placeSize);

#endif

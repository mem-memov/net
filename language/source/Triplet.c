#include "Triplet.h"

struct Triplet
{
	size_t id;
	struct Objects * objects;
	struct Parameters * parameters;
	struct Values * values;
};

struct Triplet * Triplet_construct(
	size_t id, 
	struct Objects * objects, 
	struct Parameters * parameters, 
	struct Values * values
) {
	struct Triplet * this = malloc(sizeof(struct Triplet));
	
	this->id = id;
	this->objects = objects;
	this->parameters = parameters;
	this->values = values;

	return this;
}

void * Triplet_destruct(struct Triplet * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

#ifndef VALUE_H
#define VALUE_H

#include <stdlib.h>
#include "Phrase.h"

struct Value;

struct Value * Value_construct(size_t id, struct Phrases * phrases);

void * Value_destruct(struct Value * this);

#endif

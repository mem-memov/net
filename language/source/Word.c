#include "Word.h"

struct Word
{
	size_t id;
	struct Characters * characters;
};

struct Word * Word_construct(size_t id, struct Characters * characters)
{
	struct Word * this = malloc(sizeof(struct Word));

	this->id = id;
	this->characters = characters;
	
	return this;
}

void * Word_destruct(struct Word * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

#include "WordParser.h"
#include <stdlib.h>
#include <string.h>

struct WordParser {
	char * string;
	size_t length;
	char separator;
};

struct WordParser * WordParser_construct(char * string)
{
	struct WordParser * this = malloc(sizeof (struct WordParser));

	this->length = strlen(string);
	
	this->string = malloc(sizeof(char) * (this->length + 1));
	
	strcpy(this->string, string);
	
	this->separator = ' ';
	
	return this;
}

void * WordParser_destruct(struct WordParser * this)
{
	if (NULL != this) {
		free(this->string);
		free(this);
	}

	return NULL;
}

void WordParser_makeCharacterParsers(
	struct WordParser * this, 
	struct CharacterParser *** characterParsers, 
	int * wordCount
) {
	// initialize word count
	
	if ( this->length == 0 ) {
		(*wordCount) = 0;
	} else {
		(*wordCount) = 1;
	}

	// count words
	
	size_t i;
	
	for (i = 0; i < this->length; i++) {
		
		if ( this->string[i] == this->separator) {
			++(*wordCount);
		}
	}
	
	// find words
	
	int wordIndex;
	
	char ** wordFirstCharacters = malloc(sizeof(char *) * (*wordCount));
	size_t * wordLengths = malloc(sizeof(size_t) * (*wordCount));
	
	for (wordIndex = 0; wordIndex < (*wordCount); ++wordIndex) {
		wordFirstCharacters[wordIndex] = NULL;
		wordLengths[wordIndex] = 0;
	}

	wordIndex = 0;
	for (i = 0; i < this->length; i++) {
		
		if ( this->string[i] == this->separator) {
			++wordIndex;
			continue;
		}
		
		if ( wordLengths[wordIndex] == 0 ) {
			wordFirstCharacters[wordIndex] = this->string + i;
		}
		
		wordLengths[wordIndex]++;
	}
	
	// copy words to word parsers
	
	(*characterParsers) = malloc(sizeof(struct CharacterParser *) * (*wordCount));
	
	
	int characterIndex;
	size_t wordLength;
	char * firstCharacter;
	
	for (wordIndex = 0; wordIndex < (*wordCount); ++wordIndex) {
		
		wordLength = wordLengths[wordIndex];
		firstCharacter = wordFirstCharacters[wordIndex];
		
		char * wordCharacters = malloc(sizeof(char) * (wordLength + 1));
		wordCharacters[wordLength] = '\0';
		
		for (characterIndex = 0; characterIndex < wordLength; ++characterIndex ) {
			wordCharacters[characterIndex] = firstCharacter[characterIndex];
		}

		*((*characterParsers) + wordIndex) = CharacterParser_construct(wordCharacters);
		
		free(wordCharacters);
	}
}
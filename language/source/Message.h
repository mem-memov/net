#ifndef MESSAGE_H
#define MESSAGE_H

#include "Author.h"
#include "Answer.h"
#include "Question.h"

struct Message;

struct Message * Message_constructAnswer(struct Author * author, struct Answer * answer);

struct Message * Message_constructQuestion(struct Author * author, struct Question * question);

void * Message_destruct(struct Message * this);

void Message_write(struct Message * this);

#endif

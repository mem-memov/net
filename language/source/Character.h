#ifndef CHARACTER_H
#define CHARACTER_H

#include <stdlib.h>
#include "Numbers.h"

struct Character;

struct Character * Character_construct(size_t id, struct Numbers * numbers);

void * Character_destruct(struct Character * this);

#endif

#include "Collection.h"
#include "Intersector.h"

struct Collection
{
	size_t * nodes;
	size_t length;
};

struct Collection * Collection_construct(size_t * nodes, size_t length)
{
	struct Collection * this = malloc(sizeof(struct Collection));
	
	this->nodes = malloc(sizeof(size_t) * length);
	this->length = length;
	
	size_t index;
	for ( index = 0; index < length; index++ ) {
		this->nodes[index] = nodes[index];
	}

	return this;
}

void * Collection_destruct(struct Collection * this)
{
	if ( NULL != this ) {
		free(this->nodes);
		free(this);
	}

	return NULL;
}

size_t Collection_getLength(struct Collection * this)
{
	return this->length;
}

char Collection_hasNode(struct Collection * this, size_t node)
{
	size_t index;
	for ( index = 0; index < this->length; index++ ) {
		if (this->nodes[index] == node) {
			return 1;
		};
	}
	
	return 0;
}

size_t Collection_getAt(struct Collection * this, size_t index)
{
	if (index < 0 || index > this->length ) {
		exit(1);
	}
	
	return this->nodes[index];
}

struct Collection * Collection_intersect(struct Collection * this, struct Collection * that)
{
	size_t length = Intersector_count(this->nodes, this->length, that->nodes, that->length);
	
	size_t * nodes = malloc(sizeof(size_t) * length);
	
	Intersector_intersect(this->nodes, this->length, that->nodes, that->length, nodes, length);
	
	struct Collection * intersection = Collection_construct(nodes, length);
	
	free(nodes);
	
	return intersection;
}
#include "../source/Imports.h"
#include <stdlib.h>

#define IMPORTS_MAX_CALLS 2

struct Imports {
	char call;
	char * method[IMPORTS_MAX_CALLS];
	void * destructed[IMPORTS_MAX_CALLS];
	struct Import * import[IMPORTS_MAX_CALLS];
};

struct Imports * Imports_mock()
{
	struct Imports * this = malloc(sizeof (struct Imports));

	this->call = 0;
	
	char i;
	for (i = 0; i < IMPORTS_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->import[i] = NULL;
	}
	
	return this;
}

void * Imports_unmock(struct Imports * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void * Imports_destruct(struct Imports * this)
{
	this->method[this->call] = "Imports_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

struct Import * Imports_make(struct Imports * this)
{
	this->method[this->call] = "Imports_make";
	
	struct Import * import = this->import[this->call];
	
	this->call++;
	
	return import;
}
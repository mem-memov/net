#include "Parsers.h"
#include <stdlib.h>

struct Parsers
{
	//
};

struct Parsers * Parsers_construct()
{
	struct Parsers * this = malloc(sizeof(struct Parsers));

	return this;
}

void * Parsers_destruct(struct Parsers * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Parser * Parsers_make(struct Parsers * this, char * string)
{
	return Parser_construct(string);
}
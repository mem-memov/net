#include "Chunk.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct Chunk {
	unsigned char * bytes;
	long int length;
	struct Chunk * previous;
};

struct Chunk * Chunk_construct(unsigned char * buffer, long int length, struct Chunk * previous)
{
	struct Chunk * this = malloc(sizeof (struct Chunk));
	
	this->bytes = malloc(sizeof(unsigned char) * length);
	this->length = length;
	this->previous = previous;

	long int i;
	for ( i = 0; i < length; i++ ) {
		this->bytes[i] = buffer[i];
	}

	return this;
}

void * Chunk_destruct(struct Chunk * this)
{
	if ( NULL != this ) {
		this->previous = Chunk_destruct(this->previous);

		free(this->bytes);

		free(this);
	}

	return NULL;
}

long int Chunk_getTotalLength(struct Chunk * this)
{
	if ( NULL == this->previous ) {
		return this->length;
	}
	
	return this->length + Chunk_getTotalLength(this->previous);
}

void Chunk_toString(struct Chunk * this, char * string)
{
	long int position = 0;
	
	if ( NULL != this->previous ) {
		Chunk_toString(this->previous, string);
		position = Chunk_getTotalLength(this->previous);
	}
	
	long int i;
	for ( i = 0; i < this->length; i++ ) {
		string[position + i] = this->bytes[i];
	}
}

char Chunk_endsWith(struct Chunk * this, char * end)
{
	size_t length = strlen(end) - 1; // counting no \0
	
	size_t diff;
	
	if (this->length >= length) {
		diff = length;
	} else {
		if ( NULL == this->previous) {
			return 0;
		}
		diff = this->length;
	}
	
	size_t i;
	for ( i = this->length - diff; i < this->length; i++ ) {
		if (this->bytes[i] != end[i]) {
			return 0;
		}
	}
	
	if ( diff < length ) {
		return Chunk_endsWith(this->previous, &end[diff]);
	}
	
	return 1;
}
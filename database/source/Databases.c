#include "Databases.h"
#include <stdlib.h>
#include "Stores.h"
#include "Collections.h"

struct Databases {
	struct Stores * stores;
	struct Collections * collections;
};

struct Databases * Databases_construct()
{
	struct Databases * this = malloc(sizeof (struct Databases));
	
	this->stores = Stores_construct();
	this->collections = Collections_construct();

	return this;
}

void * Databases_destruct(struct Databases * this)
{
	if (NULL != this) {
		this->stores = Stores_destruct(this->stores);
		this->collections = Collections_destruct(this->collections);
		free(this);
	}

	return NULL;
}

struct Database * Databases_make(struct Databases * this, size_t graphSize, size_t placeSize)
{
	return Database_construct(
		Stores_make(this->stores, graphSize, placeSize),
		this->collections
	);
}

#include "Phrase.h"

struct Phrase
{
	size_t id;
	struct Words * words;
};

struct Phrase * Phrase_construct(size_t id, struct Words * words)
{
	struct Phrase * this = malloc(sizeof(struct Phrase));
	
	this->id = id;
	this->words = words;

	return this;
}

void * Phrase_destruct(struct Phrase * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

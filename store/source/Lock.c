#include "Lock.h"
#include <stdlib.h>
#include <pthread.h>

struct Lock {
	//
};

struct Lock * Lock_construct()
{
	struct Lock * this = malloc(sizeof (struct Lock));

	return this;
}

void * Lock_destruct(struct Lock * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void Lock_holdConnection(struct Lock * this, size_t origin, size_t destination)
{
	
}

void Lock_releaseConnection(struct Lock * this, size_t origin, size_t destination)
{
	
}

void Lock_holdReadingNode(struct Lock * this, size_t node)
{
	
}

void Lock_releaseReadingNode(struct Lock * this, size_t node)
{
	
}

void Lock_holdRemovingNode(struct Lock * this, size_t node)
{
	
}

void Lock_releaseRemovingNode(struct Lock * this, size_t node)
{
	
}

void Lock_holdGraph(struct Lock * this)
{
	
}

void Lock_releaseGraph(struct Lock * this)
{
	
}
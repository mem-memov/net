#include "Locks.h"
#include <stdlib.h>

struct Locks {
	//
};

struct Locks * Locks_construct()
{
	struct Locks * this = malloc(sizeof (struct Locks));

	return this;
}

void * Locks_destruct(struct Locks * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Lock * Locks_make(struct Locks * this)
{
	return Lock_construct();
}
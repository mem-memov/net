#include "Graph.h"
#include "Export.h"
#include "Import.h"
#include "Boat.h"
#include "Imports.h"
#include "Knife.h"
#include "Link.h"
#include "Node.h"

struct Graph {
	unsigned char * bytes;
	
	// pool
	struct Node * node;
	struct Node * originNode;
	struct Node * destinationNode;
	struct Link * link;
	
	struct Boat * boat;
	struct Knife * knife;
	
	struct Imports * imports;
	
	//
	struct Factory * factory;
};

struct Graph * Graph_factory(struct Factory * factory, unsigned char * bytes, struct Boat * boat)
{
	struct Graph * this = Graph_construct(
		bytes,
		Factory_makeNode(factory),
		Factory_makeNode(factory),
		Factory_makeNode(factory),
		Factory_makeLink(factory),
		boat,
		Factory_makeKnife(factory, boat),
		Factory_makeImports(factory)
	);
	
	this->factory = factory;
	
	return this;
}

struct Graph * Graph_construct(
    unsigned char * bytes,
    void * node,
    void * originNode,
    void * destinationNode,
    void * link,
    void * boat,
    void * knife,
    void * imports
) {
	struct Graph * this = malloc(sizeof(struct Graph));
	
	this->bytes = bytes;

	// pool
	this->node = (struct Node *)node;
	this->originNode = (struct Node *)originNode;
	this->destinationNode = (struct Node *)destinationNode;
	this->link = (struct Link *)link;

	this->boat = (struct Boat *)boat;
	this->knife = (struct Knife *)knife;
	
	this->imports = (struct Imports *)imports;

	return this;
}

void * Graph_destruct(struct Graph * this)
{
	if ( NULL != this ) {
		free(this->bytes);
		this->bytes = NULL;

		// pool
		this->node = Node_destruct(this->node);
		this->originNode = Node_destruct(this->originNode);
		this->destinationNode = Node_destruct(this->destinationNode);
		this->link = Link_destruct(this->link);

		// fields
		this->boat = Boat_destruct(this->boat);
		
		this->knife = Knife_destruct(this->knife);
		
		//
		this->factory = Factory_destruct(this->factory);

		free(this);
	}

	return NULL;
}

char Graph_hasFreePlace(struct Graph * this)
{
	return Boat_hasSpaceForEntry(this->boat);
}

size_t Graph_addNode(struct Graph * this)
{
	size_t place = Boat_createNodeEntry(this->boat);
	
	Node_create(this->node, place);

	return place;
}

void Graph_removeNode(struct Graph * this, size_t place)
{
	Node_read(this->node, place);
	Node_delete(this->node);
	
	Boat_deleteNodeEntry(this->boat, place);
}

void Graph_connectNodes(struct Graph * this, size_t origin, size_t destination)
{
	Node_read(this->originNode, origin);
	Node_read(this->destinationNode, destination);

	size_t link = Boat_createLinkEntry(this->boat);
	Link_create(this->link, link, origin, destination);

	Node_addOutgoingLink(this->originNode, this->link);
	Node_addIncomingLink(this->destinationNode, this->link);
}

void Graph_disconnectNodes(struct Graph * this, size_t origin, size_t destination)
{
	Node_read(this->originNode, origin);
	Node_read(this->destinationNode, destination);
	
	if ( Node_isSmallOrigin(this->originNode, this->destinationNode) ) {
		
		Knife_cutDestination(this->knife, this->originNode, this->destinationNode);

	} else {

		Knife_cutOrigin(this->knife, this->originNode, this->destinationNode);

	}
}

void Graph_getNodeDestinations(struct Graph * this, size_t origin, size_t ** destinations, size_t * length)
{
	Node_read(this->node, origin);
	
	Node_getNodeDestinations(this->node, destinations, length);
}

void Graph_getNodeOrigins(struct Graph * this, size_t destination, size_t ** origins, size_t * length)
{
	Node_read(this->node, destination);
	
	Node_getNodeOrigins(this->node, origins, length);
}


size_t Graph_countNodeDestinations(struct Graph * this, size_t origin)
{
	Node_read(this->node, origin);
	
	return Node_countDestinations(this->node);
}

size_t Graph_countNodeOrigins(struct Graph * this, size_t destination)
{
	Node_read(this->node, destination);
	
	return Node_countOrigins(this->node);
}

char Graph_isNode(struct Graph * this, size_t place)
{
	if ( ! Boat_isCovering(this->boat, place)) {
		return 0;
	}
	
	Node_read(this->node, place);

	return Node_isNode(this->node);
}

void Graph_export(struct Graph * this, FILE * file)
{
	struct Export * export = Boat_createExport(this->boat);
	
	Export_write(export, file);
	
	export = Export_destruct(export);
}

void Graph_import(struct Graph * this, FILE * file)
{
	struct Import * import = Imports_make(this->imports);
	
	Import_read(import, file);
	
	import = Import_destruct(import);
}

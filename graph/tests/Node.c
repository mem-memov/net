#include "../source/Node.h"
#include <stdlib.h>

#define NODE_MAX_CALLS 4

struct Node
{
	char call;
	char * method[NODE_MAX_CALLS];
	void * destructed[NODE_MAX_CALLS];
	size_t place[NODE_MAX_CALLS];
	char isNode[NODE_MAX_CALLS];
	struct Link * link[NODE_MAX_CALLS];
	size_t destinationNode[NODE_MAX_CALLS];
	size_t deletedOutgoingLink[NODE_MAX_CALLS];
	size_t originNode[NODE_MAX_CALLS];
	size_t deletedIncomingLink[NODE_MAX_CALLS];
	size_t ** destinations[NODE_MAX_CALLS];
	size_t * length[NODE_MAX_CALLS];
	size_t ** origins[NODE_MAX_CALLS];
	char isSmallOrigin[NODE_MAX_CALLS];
	struct Node * destination[NODE_MAX_CALLS];
	size_t * outgoingLinkCount[NODE_MAX_CALLS];
	size_t * incomingLinkCount[NODE_MAX_CALLS];
};

struct Node * Node_mock()
{
	struct Node * this = malloc(sizeof(struct Node));

	this->call = 0;
	
	char i;
	for (i = 0; i < NODE_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->place[i] = 55555;
		this->isNode[i] = 27;
		this->link[i] = NULL;
		this->destinationNode[i] = 55555;
		this->deletedOutgoingLink[i] = 55555;
		this->originNode[i] = 55555;
		this->deletedIncomingLink[i] = 55555;
		this->destinations[i] = NULL;
		this->length[i] = NULL;
		this->origins[i] = NULL;
		this->isSmallOrigin[i] = 17;
		this->destination[i] = NULL;
		this->outgoingLinkCount[i] = 25;
		this->incomingLinkCount[i] = 27;
	}
	
	return this;
}

void * Node_unmock(struct Node * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

void * Node_destruct(struct Node * this)
{
	this->method[this->call] = "Node_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}


void Node_create(struct Node * this, size_t place)
{
	this->method[this->call] = "Node_create";
	this->place[this->call] = place;
	
	this->call++;
}

void Node_read(struct Node * this, size_t place)
{
	this->method[this->call] = "Node_read";
	this->place[this->call] = place;
	
	this->call++;
}

void Node_delete(struct Node * this)
{
	this->method[this->call] = "Node_delete";
	
	this->call++;
}

size_t Node_getPlace(struct Node * this)
{
	this->method[this->call] = "Node_getPlace";
	
	char place = this->place[this->call];
	
	this->call++;
	
	return place;
}

char Node_isNode(struct Node * this)
{
	this->method[this->call] = "Node_isNode";
	
	char isNode = this->isNode[this->call];
	
	this->call++;
	
	return isNode;
}

void Node_addIncomingLink(struct Node * this, struct Link * link)
{
	this->method[this->call] = "Node_addIncomingLink";
	this->link[this->call] = link;
	
	this->call++;
}

void Node_addOutgoingLink(struct Node * this, struct Link * link)
{
	this->method[this->call] = "Node_addOutgoingLink";
	this->link[this->call] = link;
	
	this->call++;
}

void Node_readOutgoingLink(struct Node * this, struct Link * link)
{
	this->method[this->call] = "Node_readOutgoingLink";
	this->link[this->call] = link;
	
	this->call++;
}

void Node_readIncomingLink(struct Node * this, struct Link * link)
{
	this->method[this->call] = "Node_readIncomingLink";
	this->link[this->call] = link;
	
	this->call++;
}

size_t Node_deleteDestination(struct Node * this, size_t destinationNode)
{
	this->method[this->call] = "Node_deleteDestination";
	this->destinationNode[this->call] = destinationNode;
	
	size_t deletedOutgoingLink = this->deletedOutgoingLink[this->call];
	
	this->call++;
	
	return deletedOutgoingLink;
}

void Node_deleteIncomingLink(struct Node * this)
{
	this->method[this->call] = "Node_deleteIncomingLink";
	
	this->call++;
}

size_t Node_deleteOrigin(struct Node * this, size_t originNode)
{
	this->method[this->call] = "Node_deleteOrigin";
	this->originNode[this->call] = originNode;
	
	size_t deletedIncomingLink = this->deletedIncomingLink[this->call];
	
	this->call++;
	
	return deletedIncomingLink;
}

void Node_deleteOutgoingLink(struct Node * this)
{
	this->method[this->call] = "Node_deleteOutgoingLink";
	
	this->call++;
}

void Node_getNodeDestinations(struct Node * this, size_t ** destinations, size_t * length)
{
	this->method[this->call] = "Node_getNodeDestinations";
	this->destinations[this->call] = destinations;
	this->length[this->call] = length;
	
	this->call++;
}

void Node_getNodeOrigins(struct Node * this, size_t ** origins, size_t * length)
{
	this->method[this->call] = "Node_getNodeOrigins";
	this->origins[this->call] = origins;
	this->length[this->call] = length;
	
	this->call++;
}

char Node_isSmallOrigin(struct Node * this, struct Node * destination)
{
	this->method[this->call] = "Node_isSmallOrigin";
	
	this->destination[this->call] = destination;
	
	size_t isSmallOrigin = this->isSmallOrigin[this->call];
	
	this->call++;
	
	return isSmallOrigin;
}

size_t Node_countDestinations(struct Node * this)
{
	this->method[this->call] = "Node_countDestinations";
	
	size_t outgoingLinkCount = this->outgoingLinkCount[this->call];
	
	this->call++;
	
	return outgoingLinkCount;
}

size_t Node_countOrigins(struct Node * this)
{
	this->method[this->call] = "Node_countOrigins";
	
	size_t incomingLinkCount = this->incomingLinkCount[this->call];
	
	this->call++;
	
	return incomingLinkCount;
}
#include "Database.h"
#include <stdlib.h>
#include "Store.h"
#include "Collection.h"
#include "Collections.h"

struct Database
{
	struct Store * store;
	//struct Entries * entries;
	struct Collections * collections;
};

struct Database * Database_construct(
	void * store,
	void * collections
) {
	struct Database * this = malloc(sizeof(struct Database));

	this->store = (struct Store *)store;
	this->collections = (struct Collections *)collections;

	return this;
}

void * Database_destruct(struct Database * this)
{
	if ( NULL != this ) {
		Store_destruct(this->store);
		free(this);
	}

	return NULL;
}

struct Collection * Database_collectNode(struct Database * this, size_t node)
{
	struct Collection * collection = Collections_make(this->collections, 1);
	
	if ( ! Store_isNode(this->store, node) ) {
		exit(1); // TODO: handle situation
	}
	
	Collection_addNode(collection, node);
	
	return collection;
}

struct Collection * Database_createNode(struct Database * this, struct Collection * origins, struct Collection * destinations)
{
	struct Collection * collection = Collections_make(this->collections, 1);

	size_t node = Store_addNode(this->store);

	Collection_addNode(collection, node);
	
	size_t index;
	size_t length;
	
	if ( NULL != origins ) {
		
		length = Collection_getLength(origins);
		
		for ( index = 0; index < length; index++ ) {
			Store_connectNodes(
				this->store, 
				Collection_getAt(origins[index]),
				node
			);
		}
	}
	
	if ( NULL != destinations ) {
		
		length = Collection_getLength(destinations);
		
		for ( index = 0; index < length; index++ ) {
			Store_connectNodes(
				this->store, 
				node,
				Collection_getAt(destinations[index])
			);
		}
	}
	
	return collection;
}

struct Collection * Database_readDestinationsOfNode(struct Database * this, size_t origin)
{
	size_t * destinations;
	size_t length;
	
	Store_getNodeDestinations(this->store, origin, &destinations, &length);

	struct Collection * collection = Collections_make(this->collections, destinations, length);
	
	free(destinations);

	return collection;
}

struct Collection * Database_readOriginsOfNode(struct Database * this, size_t destination)
{
	size_t * origins;
	size_t length;
	
	Store_getNodeOrigins(this->store, destination, &origins, &length);

	struct Collection * collection = Collections_make(this->collections, origins, length);
	
	free(origins);

	return collection;
}


#include "../source/StarError.h"

#define STARERROR_MAX_CALLS 2

struct StarError
{
	char call;
	char * method[STARERROR_MAX_CALLS];
	void * destructed[STARERROR_MAX_CALLS];
	size_t outgoingLink[STARERROR_MAX_CALLS];
	size_t incomingLink[STARERROR_MAX_CALLS];
	size_t index[STARERROR_MAX_CALLS];
	size_t maxIndex[STARERROR_MAX_CALLS];
};

struct StarError * StarError_mock()
{
	struct StarError * this = malloc(sizeof(struct StarError));
	
	this->call = 0;
	
	char i;
	for (i = 0; i < STARERROR_MAX_CALLS; i++) {
		this->method[i] = "method never called";
		this->destructed[i] = "wrong pointer";
		this->outgoingLink[i] = 55555;
		this->incomingLink[i] = 55555;
		this->index[i] = 55555;
		this->maxIndex[i] = 55555;
	}

	return this;
}

void * StarError_unmock(struct StarError * this)
{
	if ( NULL != this ) {
		free(this);
	}

	return NULL;
}

void * StarError_destruct(struct StarError * this)
{
	this->method[this->call] = "StarError_destruct";
	
	void * destructed = this->destructed[this->call];
	
	this->call++;
	
	return destructed;
}

void StarError_forbidShortDestinationList(struct StarError * this, size_t outgoingLink, size_t index, size_t maxIndex)
{
	this->method[this->call] = "StarError_forbidShortDestinationList";
	this->outgoingLink[this->call] = outgoingLink;
	this->index[this->call] = index;
	this->maxIndex[this->call] = maxIndex;
	
	this->call++;
}

void StarError_forbidLongDestinationList(struct StarError * this, size_t outgoingLink)
{
	this->method[this->call] = "StarError_forbidLongDestinationList";
	this->outgoingLink[this->call] = outgoingLink;
	
	this->call++;
}

void StarError_forbidShortOriginList(struct StarError * this, size_t incomingLink, size_t index, size_t maxIndex)
{
	this->method[this->call] = "StarError_forbidShortOriginList";
	this->incomingLink[this->call] = incomingLink;
	this->index[this->call] = index;
	this->maxIndex[this->call] = maxIndex;
	
	this->call++;
}

void StarError_forbidLongOriginList(struct StarError * this, size_t incomingLink)
{
	this->method[this->call] = "StarError_forbidLongOriginList";
	this->incomingLink[this->call] = incomingLink;
	
	this->call++;
}
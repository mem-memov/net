#include "Graphs.h"
#include <stdlib.h>
#include "Factory.h"
#include "Boat.h"

struct Graphs {
	
};

struct Graphs * Graphs_construct()
{
	struct Graphs * this = malloc(sizeof (struct Graphs));

	return this;
}

void * Graphs_destruct(struct Graphs * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Graph * Graphs_make(struct Graphs * this, size_t graphSize, size_t placeSize)
{
	if (graphSize < 1) {
		exit(0);
	}
	if (placeSize < 1) {
		exit(0);
	}

	size_t entrySize = 6;
	unsigned char * bytes = malloc(graphSize * entrySize * placeSize);
	
	struct Factory * factory = Factory_construct(bytes, graphSize, entrySize, placeSize);
	
	struct Boat * boat = Factory_makeBoat(factory);
	Boat_create(boat);
	
	struct Graph * graph = Graph_factory(factory, bytes, boat);
	
	return graph;
}
#include "Triplets.h"
#include <stdlib.h>

struct Triplets
{
	struct Objects * objects;
	struct Parameters * parameters;
	struct Values * values;
	struct Database * database;
};

struct Triplets * Triplets_construct(
	struct Objects * objects, 
	struct Parameters * parameters, 
	struct Values * values, 
	struct Database * database
) {
	struct Triplets * this = malloc(sizeof(struct Triplets));
	
	this->objects = objects;
	this->parameters = parameters;
	this->values = values;
	
	this->database = database;

	return this;
}

void * Triplets_destruct(struct Triplets * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Triplet * Triplets_make(struct Triplets * this, struct Parser * parser)
{
	struct Object * object = Objects_make(this->objects, parser);
	struct Parameter * parameter = Parameters_make(this->parameters, parser);
	struct Value * value = Values_make(this->values, parser);
	
	size_t id = 1;
	
	return Triplet_construct(id, this->objects, this->parameters, this->values);
}

#ifndef PARSER_H
#define PARSER_H

#include "Phrases.h"
#include "WordParser.h"

struct Parser;

struct Parser * Parser_construct(char * string);

void * Parser_destruct(struct Parser * this);

char Parser_isQuestion(struct Parser * this);

struct WordParser * Parser_makeWordParser(struct Parser * this, char phraseIndex);

#endif

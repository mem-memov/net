#ifndef PHRASE_H
#define PHRASE_H

#include <stdlib.h>
#include "Words.h"

struct Phrase;

struct Phrase * Phrase_construct(size_t id, struct Words * words);

void * Phrase_destruct(struct Phrase * this);

#endif

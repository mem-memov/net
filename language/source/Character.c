#include "Character.h"

struct Character
{
	size_t id;
	struct Numbers * numbers;
};

struct Character * Character_construct(size_t id, struct Numbers * numbers)
{
	struct Character * this = malloc(sizeof(struct Character));

	this->id = id;
	this->numbers = numbers;

	return this;
}

void * Character_destruct(struct Character * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

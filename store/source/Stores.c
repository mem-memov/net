#include "Stores.h"
#include <stdlib.h>
#include "Graph.h"
#include "Graphs.h"
#include "Locks.h"

struct Stores {
	struct Graphs * graphs;
	struct Locks * locks;
};

struct Stores * Stores_construct()
{
	struct Stores * this = malloc(sizeof (struct Stores));
	
	this->graphs = Graphs_construct();
	this->locks = Locks_construct();

	return this;
}

void * Stores_destruct(struct Stores * this)
{
	if (NULL != this) {
		this->graphs = Graphs_destruct(this->graphs);
		this->locks = Locks_destruct(this->locks);
		free(this);
	}

	return NULL;
}

struct Store * Stores_make(struct Stores * this, size_t graphSize, size_t placeSize)
{
	return Store_construct(
		Graphs_make(this->graphs, graphSize, placeSize), 
		Locks_make(this->locks)
	);
}
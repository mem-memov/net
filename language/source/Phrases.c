#include "Phrases.h"
#include <stdlib.h>
#include "CharacterParser.h"
#include "Word.h"

struct Phrases
{
	struct Words * words;
	struct Database * database;
};

struct Phrases * Phrases_construct(struct Words * words, struct Database * database)
{
	struct Phrases * this = malloc(sizeof(struct Phrases));
	
	this->words = words;
	
	this->database = database;

	return this;
}

void * Phrases_destruct(struct Phrases * this)
{
	if (NULL != this) {
		free(this);
	}

	return NULL;
}

struct Phrase * Phrases_make(struct Phrases * this, struct WordParser * wordParser)
{
	int wordCount;
	struct CharacterParser ** characterParsers;
	
	WordParser_makeCharacterParsers(wordParser, &characterParsers, &wordCount);
	
	struct Word ** words = malloc(sizeof(struct Word *) * wordCount);
	
	int i;
	for ( i = 0; i < wordCount; ++i ) {
		words[i] = Words_make(this->words, characterParsers[i]);
	}
	
	size_t id = 1;

	return Phrase_construct(id, this->words);
}

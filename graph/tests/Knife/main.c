#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../../source/Knife.c"
#include "../Boat.c"
#include "../Node.c"

struct Knife * knife;
struct Boat * boat;

void prepareTest()
{
	boat = Boat_mock();
	
	knife = Knife_construct(boat);
}

void demolishTest()
{
	Knife_destruct(knife);
	
	boat = Boat_unmock(boat);
}

void it_cuts_connection_after_search_for_specific_destination()
{
	prepareTest();
	
	size_t destinationPlace = 60;
	size_t deletedOutgoingLinkPlace = 120;
	
	struct Node * originNode = Node_mock();
	
	originNode->deletedOutgoingLink[0] = deletedOutgoingLinkPlace;
	
	struct Node * destinationNode = Node_mock();

	destinationNode->place[0] = destinationPlace;
	
	Knife_cutDestination(knife, originNode, destinationNode);
	
	assert(0 == strcmp(destinationNode->method[0], "Node_getPlace"));
	
	assert(
		0 == strcmp(originNode->method[0], "Node_deleteDestination") 
		&& originNode->destinationNode[0] == destinationPlace
	);
	
	assert(0 == strcmp(destinationNode->method[1], "Node_deleteIncomingLink"));
	
	assert(
		0 == strcmp(boat->method[0], "Boat_deleteLinkEntry") 
		&& boat->place[0] == deletedOutgoingLinkPlace
	);
	
	demolishTest();
}

void it_skips_cutting_missing_destination_connection()
{
	prepareTest();
	
	size_t destinationPlace = 60;
	size_t deletedOutgoingLinkPlace = 0;
	
	struct Node * originNode = Node_mock();
	
	originNode->deletedOutgoingLink[0] = deletedOutgoingLinkPlace;
	
	struct Node * destinationNode = Node_mock();
	
	destinationNode->place[0] = destinationPlace;
	
	Knife_cutDestination(knife, originNode, destinationNode);
	
	assert(0 == strcmp(destinationNode->method[0], "Node_getPlace"));
	
	assert(
		0 == strcmp(originNode->method[0], "Node_deleteDestination") 
		&& originNode->destinationNode[0] == destinationPlace
	);
	
	assert(0 == strcmp(destinationNode->method[1], "method never called"));
	assert(0 == strcmp(boat->method[0], "method never called"));
	
	demolishTest();
}

void it_cuts_connection_after_search_for_specific_origin()
{
	prepareTest();
	
	size_t originPlace = 36;
	size_t deletedIncomingLinkPlace = 120;
	
	struct Node * originNode = Node_mock();
	
	originNode->place[0] = originPlace;
	
	struct Node * destinationNode = Node_mock();
	
	destinationNode->deletedIncomingLink[0] = deletedIncomingLinkPlace;
	
	Knife_cutOrigin(knife, originNode, destinationNode);
	
	assert(0 == strcmp(originNode->method[0], "Node_getPlace"));
	
	assert(
		0 == strcmp(destinationNode->method[0], "Node_deleteOrigin") 
		&& destinationNode->originNode[0] == originPlace
	);
	
	assert(0 == strcmp(originNode->method[1], "Node_deleteOutgoingLink"));
	
	assert(
		0 == strcmp(boat->method[0], "Boat_deleteLinkEntry") 
		&& boat->place[0] == deletedIncomingLinkPlace
	);
	
	demolishTest();
}

void it_skips_cutting_missing_origin_connection()
{
	prepareTest();
	
	size_t originPlace = 36;
	size_t deletedIncomingLinkPlace = 0;
	
	struct Node * originNode = Node_mock();
	
	originNode->place[0] = originPlace;
	
	struct Node * destinationNode = Node_mock();
	
	destinationNode->deletedIncomingLink[0] = deletedIncomingLinkPlace;
	
	Knife_cutOrigin(knife, originNode, destinationNode);
	
	assert(0 == strcmp(originNode->method[0], "Node_getPlace"));
	
	assert(
		0 == strcmp(destinationNode->method[0], "Node_deleteOrigin") 
		&& destinationNode->originNode[0] == originPlace
	);
	
	assert(0 == strcmp(originNode->method[1], "method never called"));
	assert(0 == strcmp(boat->method[0], "method never called"));
	
	demolishTest();
}

int main(int argc, char** argv)
{
	it_cuts_connection_after_search_for_specific_destination();
	it_skips_cutting_missing_destination_connection();
	it_cuts_connection_after_search_for_specific_origin();
	it_skips_cutting_missing_origin_connection();

	return (EXIT_SUCCESS);
}
#ifndef MESSAGES_H
#define MESSAGES_H

#include "Message.h"
#include "Authors.h"
#include "Answers.h"
#include "Database.h"
#include "Parser.h"
#include "Questions.h"

struct Messages;

struct Messages * Messages_construct(
    struct Authors * authors, 
    struct Answers * answers, 
    struct Questions * questions, 
    struct Database * database
);

void * Messages_destruct(struct Messages * this);

struct Message * Messages_make(struct Messages * this, struct Parser * parser);

#endif

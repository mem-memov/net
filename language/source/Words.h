#ifndef WORDS_H
#define WORDS_H

#include "Database.h"
#include "Characters.h"
#include "CharacterParser.h"
#include "Word.h"

struct Words;

struct Words * Words_construct(struct Characters * characters, struct Database * database);

void * Words_destruct(struct Words * this);

struct Word * Words_make(struct Words * this, struct CharacterParser * caracterParser);

#endif
